﻿using Azure.Core;

using FFE.Models.Message;
using FFE.Models.UsersModels;
using FFE.Services;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FFE.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MessageController : BaseController
    {
        private readonly IChatActionsService _chatActionsService;
        public MessageController(IChatActionsService chatActionsService)
        {
            _chatActionsService = chatActionsService;
        }

        /// <summary>
        /// đổi trạng thái người dùng
        /// </summary>
        /// <returns></returns>
        [HttpPost("SaveStatusOnline")]
        public async Task<ActionResult> SaveStatusOnline(SaveStatusConnectRequestModel request)
        {
            var response = await _chatActionsService.SaveChangeStatus(request);
            return Ok(response);
        }

        /// <summary>
        /// danh sách user, sort theo người online
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpGet("GetUsersOnline")]
        public async Task<ActionResult> GetUsersOnline(UsersOnlineRequestModel request)
        {
            var response = await _chatActionsService.GetPagingUserOnline(request, GetUserEmail());
            return Ok(response);
        }

        /// <summary>
        /// gửi tin nhắn đến 1 người
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpPost("SaveMessengerToUser")]
        public async Task<ActionResult> SaveMessengerToUser(SaveMessageToUserRequestModel request)
        {
            var response = await _chatActionsService.SaveMessengerToUser(request, GetUserEmail(), Convert.ToInt64(GetUserId()));
            return Ok(response);
        }

        /// <summary>
        /// gửi tin nhắn đến 1 nhóm
        /// </summary>
        /// <returns></returns>
        [HttpPost("SendMessengerToGroup")]
        public async Task<ActionResult> SendMessengerToGroup()
        {
            return Ok("gửi tin nhắn đến 1 nhóm");
        }

        /// <summary>
        /// danh sách tin nhắn tới 1 người 
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpPost("GetPagingMessageToUser")]
        public async Task<ActionResult> GetPagingMessageToUser(MessageToUserRequestModel request)
        {
            var response = await _chatActionsService.GetPagingMessageToUser(request, GetUserEmail(), Convert.ToInt64(GetUserId()));
            return Ok(response);
        }

        /// <summary>
        /// danh sách tin nhắn với mọi người 
        /// </summary>
        /// <returns></returns>
        [HttpPost("GetPagingMyListMessage")]
        public async Task<ActionResult> GetPagingMyListMessage()
        {
            return Ok("danh sách tin nhắn với mọi người ");
        }
    }
}
