﻿using Microsoft.AspNetCore.Mvc;

namespace FFE.Controllers
{
    public class BaseController : ControllerBase
    {
        protected string GetUserEmail()
        {
            return User.Claims.First(x => x.Type == "Username").Value;
        }

        protected string GetUserId()
        {
            return User.Claims.First(x => x.Type == "UserId").Value;
        }

        protected string GetFullname()
        {
            return User.Claims.First(x => x.Type == "Fullname").Value;
        }

        protected string CountTotalMember()
        {
            return User.Claims.First(x => x.Type == "TotalMember").Value;
        }

        protected string GetTokenAppRaw()
        {
            return User.Claims.First(x => x.Type == "TokenAppRaw").Value;
        }

        protected string GetRefreshTokenAppRaw()
        {
            return User.Claims.First(x => x.Type == "RefreshTokenAppRaw").Value;
        }
    }
}
