﻿using FFE.Models.Posts;
using FFE.Models.Transaction;
using FFE.Services;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FFE.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionController : BaseController
    {
        private readonly ITransactionService _transactionService;
        public TransactionController(ITransactionService transactionService)
        {
            _transactionService = transactionService;
        }

        [Authorize]
        [HttpPost("GetPagingMyTransaction")]
        public async Task<ActionResult> GetPagingMyTransaction(TransactionRequestModel request)
        {
            var output = await _transactionService.GetPagingTransaction(request, GetUserEmail());
            return Ok(output);
        }

        [Authorize]
        [HttpPost("SearchUsersReceiveToken")]
        public async Task<ActionResult> SearchUsersReceiveToken(SearchUsersTransactionRequestModel request)
        {
            var output = await _transactionService.SearchUsersReceiveToken(request, GetUserEmail());
            return Ok(output);
        }

        [Authorize]
        [HttpPost("SendToken")]
        public async Task<ActionResult> SendToken(SendTokenTransactionRequestModel request)
        {
            var output = await _transactionService.SendToken(request, GetUserEmail());
            return Ok(output);
        }

        [Authorize]
        [HttpPost("ClaimEarnToken")]
        public async Task<ActionResult> ClaimEarnToken(ClaimTokenEarnedRequestModel request)
        {
            var output = await _transactionService.ClaimEarnToken(request.Amount, GetUserEmail());
            return Ok(output);
        }
    }
}
