﻿using FFE.Models;
using FFE.Models.AuthenticationModels;
using FFE.Services;
using FFE.Services.Interfaces;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

using System.Security.Claims;

namespace FFE.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthenticationController : BaseController
    {
        private readonly IAuthenticationService _authenticationService;
        private readonly IServiceAPI _serviceAPI;

        public AuthenticationController(IAuthenticationService authenticationService, IServiceAPI serviceAPI)
        {
            _authenticationService = authenticationService;
            _serviceAPI = serviceAPI;
        }

        [HttpPost("Login")]
        public async Task<ActionResult> Login([FromBody] LoginRequestModel request)
        {
            var output = await _authenticationService.Login(request);
            return Ok(output);
        }

        [HttpPost("IsExistedRefCode")]
        public async Task<ActionResult> IsExistedRefCode([FromBody] RefCodeRequestModel request)
        {
            var output = await _authenticationService.IsExistedRefCode(request);
            return Ok(output);
        }

        [HttpPost("SignUp")]
        public async Task<ActionResult> SignUp([FromBody] SignUpRequestModel request)
        {
            var output = await _authenticationService.SignUp(request);
            return Ok(output);
        }

        [HttpPost("ConfirmOTPMailSignUp")]
        public async Task<ActionResult> ConfirmOTPMail([FromBody] ConfirmOTPRequestModel request)
        {
            var output = await _authenticationService.ConfirmOTPMailSignUp(request);
            return Ok(output);
        }

        [HttpPost("ForgetPassword")]
        public async Task<ActionResult> ForgetPassword([FromBody] VerifyEmailRequestModel request)
        {
            var output = await _authenticationService.ForgetPassword(request);
            return Ok(output);
        }

        [HttpPost("VerifyEmail")]
        public async Task<ActionResult> VerifyEmail([FromBody] VerifyEmailRequestModel request)
        {
            var output = await _authenticationService.VerifyEmail(request);
            return Ok(output);
        }
        [HttpPost("ConfirmOTPForgotPassword")]
        public async Task<ActionResult> ConfirmOTPForgotPassword([FromBody] ConfirmOTPRequestModel request)
        {
            var output = await _authenticationService.ConfirmOTPForgotPassword(request);
            return Ok(output);
        }
        [HttpPost("SetNewPassword")]
        public async Task<ActionResult> SetNewPassword([FromBody] SetNewPasswordRequestModel request)
        {
            var output = await _authenticationService.SetNewPassword(request);
            return Ok(output);
        }

        [HttpGet("SyncUserAppRaw")]
        public async Task<ActionResult> SyncUserAppRaw(string? passcode)
        {
            var output = await _authenticationService.SyncUserAppRaw(passcode);
            return Ok(output);
        }
        //[Authorize]
        //[HttpGet("GetListUsers")]
        //public async Task<ActionResult> GetListUsers()
        //{
        //    var output = await _serviceAPI.GetListUsers();
        //    return Ok(output);
        //}

        //[Authorize]
        //[HttpGet("GetTokenClaims")]
        //public async Task<ActionResult> GetTokenClaims()
        //{
        //    var mail = GetUserEmail();
        //    var fullname = GetFullname();
        //    return Ok(new { mail, fullname});
        //}
    }
}
