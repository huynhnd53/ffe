﻿using FFE.Services;
using FFE.Services.Interfaces;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

using static FFE.Common.EnumBase;

namespace FFE.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EarnController : BaseController
    {
        private readonly IEarningActionService _earningActionService;

        public EarnController(IEarningActionService earningActionService)
        {
            _earningActionService = earningActionService;
        }


        [Authorize]
        [HttpPost("StartEarning")]
        public async Task<ActionResult> StartEarning()
        {
            var output = await _earningActionService.StartEarning(GetUserEmail());
            return Ok(output);
        }
        [Authorize]
        [HttpPost("HasStartEarn")]
        public async Task<ActionResult> StartEarng()
        {
            var output = await _earningActionService.HasStartEarn(GetUserEmail());
            return Ok(output);
        }
        //[Authorize]
        //[HttpPost("StartEarn")]
        //public async Task<ActionResult> StartEarn()
        //{
        //    var output = await _earningActionService.ChangeEarningAction(EnumEarningAction.Start, GetUserEmail());
        //    return Ok(output);
        //}

        //[Authorize]
        //[HttpPost("StopEarn")]
        //public async Task<ActionResult> StopEarn()
        //{
        //    var output = await _earningActionService.ChangeEarningAction(EnumEarningAction.Stop, GetUserEmail());
        //    return Ok(output);
        //}
    }
}
