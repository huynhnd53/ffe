﻿using FFE.Models.Transaction;
using FFE.Services;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FFE.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WalletController : BaseController
    {
        private readonly IWalletService _walletService;
        public WalletController(IWalletService walletService)
        {
            _walletService = walletService;
        }

        [Authorize]
        [HttpGet("MyWalletDetail")]
        public async Task<ActionResult> MyWalletDetail()
        {
            var output = await _walletService.MyWalletDetail(GetUserEmail());
            return Ok(output);
        }

        [Authorize]
        [HttpGet("MyListWalletDetail")]
        public async Task<ActionResult> MyListWalletDetail()
        {
            var output = await _walletService.MyListWalletDetail(GetUserEmail());
            return Ok(output);
        }
    }
}
