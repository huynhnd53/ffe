﻿using FFE.Models.Posts;
using FFE.Services;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FFE.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : BaseController
    {
        private readonly IProductService _productService;
        public ProductController(IProductService productService)
        {
            _productService = productService;
        }

        [HttpPost("GetPagingProduct")]
        public async Task<ActionResult> GetPagingPost(ProductRequestModel request)
        {
            var output = await _productService.GetPagingProduct(request);
            return Ok(output);
        }
    }
}
