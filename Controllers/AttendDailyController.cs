﻿using FFE.Models.AttendedDaily;
using FFE.Services;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FFE.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AttendDailyController : BaseController
    {
        private readonly IAttendedDailyService _attendedDailyService;
        public AttendDailyController(IAttendedDailyService attendedDailyService)
        {
            _attendedDailyService = attendedDailyService;
        }


        [Authorize]
        [HttpGet("MyAttendedMonthly")]
        public async Task<ActionResult> MyAttendedMonthly(int month, int year)
        {
            var output = await _attendedDailyService.UsersAttendedMonth(month, year, GetUserEmail());
            return Ok(output);
        }

        [Authorize]
        [HttpPost("UserAttendedDaily")]
        public async Task<ActionResult> UserAttendedDaily(AttendedDailyRequestModel request)
        {
            var output = await _attendedDailyService.UserAttendedDaily(request.Day, request.Month, request.Year, GetUserEmail());
            return Ok(output);
        }
    }
}
