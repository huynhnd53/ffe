﻿using FFE.Models.Notification;
using FFE.Models.Transaction;
using FFE.Services;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FFE.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NotificationController : BaseController
    {
        private readonly INotificationService _notificationService;
        public NotificationController(INotificationService notificationService)
        {
            _notificationService = notificationService;
        }

        [Authorize]
        [HttpPost("GetPagingMyNotification")]
        public async Task<ActionResult> GetPagingMyTransaction(NotificationRequestModel request)
        {
            var output = await _notificationService.GetPagingMyNoti(request, GetUserEmail());
            return Ok(output);
        }

        [Authorize]
        [HttpPost("MaskReadNoti")]
        public async Task<ActionResult> MaskReadNoti(MaskReadNotiRequestModel request)
        {
            var output = await _notificationService.MaskReadNoti(request.NotificationId, GetUserEmail());
            return Ok(output);
        }

        [Authorize]
        [HttpPost("MaskReadAllNoti")]
        public async Task<ActionResult> MaskReadAllNoti()
        {
            var output = await _notificationService.MaskReadAllNoti(GetUserEmail());
            return Ok(output);
        }
    }
}
