﻿using FFE.Models.Posts;
using FFE.Models;
using FFE.Services;
using Microsoft.AspNetCore.Mvc;


namespace FFE.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PostsController : BaseController
    {
        private readonly IPostService _postService;
        public PostsController(IPostService postService)
        {
            _postService = postService;
        }

        [HttpPost("GetPagingPost")]
        public async Task<ActionResult> GetPagingPost(PostRequestModel request)
        {
            var output = await _postService.GetPagingPost(request);
            return Ok(output);
        }
    }
}
