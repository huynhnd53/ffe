﻿using FFE.Models.AuthenticationModels;
using FFE.Models.Transaction;
using FFE.Services;
using FFE.Services.Interfaces;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FFE.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : BaseController
    {
        private readonly IServiceAPI _serviceAPI;
        private readonly IAuthenticationService _authenticationService;

        public UsersController(IServiceAPI serviceAPI, IAuthenticationService authenticationService)
        {
            _serviceAPI = serviceAPI;
            _authenticationService = authenticationService;
        }

        [Authorize]
        [HttpGet("GetUserInfo")]
        public async Task<ActionResult> GetUserInfo()
        {
            var output = await _authenticationService.UserInfo(GetUserEmail(), GetTokenAppRaw());
            return Ok(output);
        }
    }
}
