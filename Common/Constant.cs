﻿namespace FFE.Common
{
    public static class Constant
    {
        public static decimal EARN_PER_MIN = 0.3M;
        public static string URL_IMAGE = "https://api.ffe.vn/Images/";
        public static string URL_IMAGE_THUMB_DEFAULT = "https://api.ffe.vn/Images/thumbDefault.png";
    }
    public class MessageConstant
    {
        public const string ERROR_SYSTEM = "có lỗi trong quá trình xử lý";
        public const string SUCCESS_SYSTEM = "Thành công";

        public const string ACCESS_DENIED = "access denined";
        public const string USER_EXPIRED_DATE = "Tài khoản hết hạn sử dụng.";
        public const string USER_EXPIRED_DATE_OVER_30Days = "Tài khoản hết hạn sử dụng vui lòng liên hệ Mr.Tùng 0966.331.380";
        public const string USER_NOT_ACTIVE = "Tài khoản tạm thời bị khóa.";
        public const string USER_INVALID = "Đăng nhập không thành công.";
        public const string USER_PASSWROD_NOTMATCH = "Mật khẩu hiện tại không chính xác.";
        public const string USER_PASSWORD_LENGTH_ERROR = "Mật khẩu phải lớn hơn hoặc bằng 6 kí tự.";
        public const string USER_PASSWORD_CHANGE_SUCCESS = "Thay đổi mật khẩu thành công.";
        public const string USER_USERNAME_INVALID = "Tài khoản không hợp lệ.";
        public const string USER_USERNAME_EXISTS = "Tài khoản đã tồn tại trong hệ thống.";
        public const string USER_USERNAME_DELETED = "Đăng nhập không thành công.";
        public const string USER_STAFF_NOTBELONG = "Tài khoản nhân viên không thuộc bạn quản lý.";
        public const string SYSTEM_ERROR = "Có lỗi trong quá trình xử lý dữ liệu.";


        public const string USER_GETSUCCESS = "Lấy dữ liệu tài khoản thành công.";
        public const string USER_GETPERMISSION = "Lấy dữ liệu quyền thành công.";
        public const string USER_ADDPERMISSIONSUCCESS = "Phân quyền thành công.";
        public const string USER_ADDPERMISSIONERROR = "Phân quyền không thành công,vui lòng thử lại";
        public const string USER_UPDATESUCCESS = "Cập nhật thành công";
        public const string USER_EXTENDSUCCESS = "Gia hạn thành công";
        public const string USER_DELETESUCCESS = "Xóa tài khoản thành công";
        public const string USER_CHILD_UPDATEERROR = "Cập nhật tài khoản nhân viên không thành công";
        public const string USER_UPDATEERROR = "Cập nhật không thành công";
        public const string USER_DONTNEDDTRAIL = "Bạn vẫn còn thời gian sử dụng, không cần dùng lượt miễn phí";

    }
}
