﻿using System.ComponentModel;

namespace FFE.Common
{
    public class EnumBase
    {
        public enum UserStatus
        {
            [Description("Khóa")]
            InActive = 0,
            [Description("Hoạt động")]
            Active = 1,
            [Description("Xóa")]
            Delete = -11,
        }
        public enum ResponseResult
        {
            [Description("Thao tác thành công")]
            Success = 1,
            [Description("Lỗi")]
            Error = -1,
            [Description("Token hết hạn")]
            TokenError = 11
        }

        public enum EnumCategories
        {
            [Description("Ví Lạnh")]
            ColdWallet = 1,
            [Description("Bài viết")]
            Post = 2,
            [Description("Sliders")]
            Sliders = 3,
            [Description("Posts")]
            Posts = 4
        }
        public enum EnumStatusPost
        {
            [Description("Hoạt động")]
            Active = 1,
            [Description("Xóa")]
            Deleted = -99
        }

        public enum EnumIsOnline
        {
            [Description("Hoạt động")]
            Online = 1,
            [Description("Tắt Hoạt động")]
            Offline = 0
        }

        public enum EnumTransactionType
        {
            [Description("Thưởng từ hệ thống")]
            ReceiveFromSystem = 1,
            [Description("Chuyển token đi")]
            SendToken = 2,
            [Description("Nhận token đi")]
            ReceiveToken = 3,
            [Description("Điểm danh")]
            AttendedDaily = 4,
        }
        public enum EnumTransactionStatus
        {
            [Description("Thành công")]
            Success = 1,
            [Description("Thất bại")]
            Fail = 1,
        }
        public enum EnumUserRole
        {
            [Description("Customer")]
            Customer = 1,
        }

        public enum EnumEarningAction
        {
            [Description("Bắt đầu đào")]
            Start = 1,
            [Description("Dừng đào")]
            Stop = 0,
            [Description("Đã đào")]
            Mined = -1,
        }


        public enum EnumToken
        {
            [Description("WREG")]
            WREG = 1,
            [Description("Tether USDt")]
            USDT = 2,
            [Description("USD Coin")]
            USDC = 3,
            [Description("Binance USD")]
            BUSD = 4,
            [Description("WREG From Earning ")]
            WREGE = 5,

        }
    }
}
