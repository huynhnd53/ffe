﻿using FFE.Services;
using FFE.Services.Interfaces;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;

namespace FFE.Common
{
    public class ChatHub : Hub
    {
        //private readonly IChatActionsService _chatActionsService;
        //private readonly ControllerBase _contextControllerBase;
        //public ChatHub (IChatActionsService chatActionsService, ControllerBase controllerBase)
        //{
        //    _chatActionsService = chatActionsService;
        //    _contextControllerBase = controllerBase;
        //}

        public async Task SendMessage(string userSend, string message)
        {
            await Clients.All.SendAsync("ReceiveMessage", userSend, message); // Gửi tin nhắn tới tất cả các client đang kết nối
        }

        public async Task SendMessageToUser(string userSend, string userReceive, string message)
        {
            await Clients.Client(userReceive).SendAsync("ReceiveMessageToUser", userSend, message);
        }

        public string GetConnectionId()
        {
            return Context.ConnectionId;
        }

        //public async Task SendMessage(string message)
        //{
        //    var userSend = _contextControllerBase.User.Claims.First(x => x.Type == "Username").Value;
        //    await Clients.All.SendAsync("ReceiveMessage", userSend, message); // Gửi tin nhắn tới tất cả các client đang kết nối
        //}

        //public async Task SendMessageToUser(string userReceive, string message)
        //{
        //    var userSend = _contextControllerBase.User.Claims.First(x => x.Type == "Username").Value;
        //    await Clients.Client(userReceive).SendAsync("ReceiveMessageToUser", userSend, message);
        //}

        //public string GetConnectionId()
        //{
        //    var username = _contextControllerBase.User.Claims.First(x => x.Type == "Username").Value;
        //    _chatActionsService.SaveChangeStatus(Context.ConnectionId, username, 1);
        //    return Context.ConnectionId;
        //}
    }
}
