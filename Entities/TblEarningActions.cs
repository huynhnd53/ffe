﻿using System.ComponentModel.DataAnnotations.Schema;

namespace FFE.Entities
{
    [Table("TblEarningActions")]
    public class TblEarningActions : IAuditableEntity
    {
        public int? ActionType { get; set; }
        public string? Username { get; set; }
    }
}
