﻿using System.ComponentModel.DataAnnotations.Schema;

namespace FFE.Entities
{
    [Table("TblProducts")]
    public class TblProducts : IAuditableEntity
    {
        public string? ProductName { get; set; }
        public string? Descriptions { get; set; }
        public string? Contents { get; set; }
        public decimal? ProductPrice { get; set; }
        public decimal? Discount { get; set; }
        public int? CategoryId { get; set; }
        public int? Status { get; set; }
        public string? Unit { get; set; }
        public string? Thumbnail { get; set; }
        public string? Images { get; set; }
    }
}
