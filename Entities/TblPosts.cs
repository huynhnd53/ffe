﻿using System.ComponentModel.DataAnnotations.Schema;

namespace FFE.Entities
{
    [Table("TblPosts")]
    public class TblPosts : IAuditableEntity
    {
        public string? Title { get; set; }
        public string? Summary { get; set; }
        public string? Contents { get; set; }
        public int? Status { get; set; }
        public int? LikedCount { get; set; }
        public int? SharedCount { get; set; }
        public int? CommentedCount { get; set; }
        public string? Thumbnail { get; set; }
        public string? Images { get; set; }
        public long? CategoriId { get; set; }
    }
}
