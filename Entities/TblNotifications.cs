﻿using System.ComponentModel.DataAnnotations.Schema;

namespace FFE.Entities
{
    [Table("TblNotifications")]
    public class TblNotifications : IAuditableEntity
    {
        public long? UserId { get; set; }
        public string? Username { get; set; }
        public int? Status { get; set; }
        public string? Contents { get; set; }
    }
}
