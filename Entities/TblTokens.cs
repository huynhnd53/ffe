﻿using System.ComponentModel.DataAnnotations.Schema;

namespace FFE.Entities
{
    [Table("TblTokens")]
    public class TblTokens : IAuditableEntity
    {
        public string? TokenName { get; set; }
        public string? TokenCode { get; set; }
        public decimal? Price { get; set; }
        //contract của token 
        public string? ContractCodeBep20 { get; set; }
        //contract của token 
        public string? ContractCodeERC20 { get; set; }
    }
}
