﻿using System.ComponentModel.DataAnnotations.Schema;

namespace FFE.Entities
{
    [Table("TblMesssage")]
    public class TblMesssage : IAuditableEntity
    {
        public long UserSourceId { get; set; } = 0;
        public string UserSourceName { get; set; } = string.Empty;
        public long UserTargetID { get; set; } = 0;
        public string UserTargetName { get; set; } = string.Empty;
        public string MesssageContent { get; set; } = string.Empty;
        public bool IsRead { get; set; } = false;
    }
}
