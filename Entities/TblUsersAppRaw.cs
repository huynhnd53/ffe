﻿using System.ComponentModel.DataAnnotations.Schema;

namespace FFE.Entities
{
    [Table("TblUsersAppRaw")]
    public class TblUsersAppRaw : IAuditableEntity
    {
        public string? _id { get; set; }
        public string? email { get; set; }
        public string? password { get; set; }
        public bool? isVerified { get; set; }
        public string? fullName { get; set; }
        public string? affiliateLevel { get; set; }
        public string? type { get; set; }
        public bool? isChangePassWord { get; set; }
        public bool? isLock { get; set; }
        public bool? isDelete { get; set; }
        public string? myRef { get; set; }
        public long? totalOrder { get; set; }
        public long? totalRevenue { get; set; }
        public long? totalMoney { get; set; }
        public long? totalPurchase { get; set; }
        public long? totalCommission { get; set; }
        public DateTime? lastLogin { get; set; }
        public DateTime? createdAt { get; set; }
        public DateTime? updatedAt { get; set; }
        public DateTime? birthDay { get; set; }
        public string? phoneNumber { get; set; }
        public long? __v { get; set; }
        public string? parentID { get; set; }
    }
}
