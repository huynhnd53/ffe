﻿using System.ComponentModel.DataAnnotations.Schema;

namespace FFE.Entities
{
    [Table("TblConfigAttendDailys")]
    public class TblConfigAttendDailys : IAuditableEntity
    {
        public DateTime AttendDate { get; set; }
        public int ConfigDay { get; set; } = 0;
        public int ConfigMonth { get; set; } = 0;
        public int ConfigYear { get; set; } = 0;
        public decimal? Points { get; set; }
    }
}
