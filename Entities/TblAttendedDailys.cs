﻿using System.ComponentModel.DataAnnotations.Schema;

namespace FFE.Entities
{
    [Table("TblAttendedDailys")]
    public class TblAttendedDailys : IAuditableEntity
    {
        public long? ConfigAttendDailyId { get; set; }
        public long? UserId { get; set; }
        public string? Username { get; set; }
        public DateTime? AttendDate { get; set; }
        public int? ConfigDay { get; set; }
        public int? ConfigMonth { get; set; }
        public int? ConfigYear { get; set; }
    }
}
