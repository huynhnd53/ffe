﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FFE.Entities
{
    [Table("TblUsers")]
    public class TblUsers : IAuditableEntity
    {
        public string? Username { get; set; }
        public string? IdAppRaw { get; set; }
        public string? Password { get; set; }
        public string? PasswordAppRaw { get; set; }
        public string? Fullname { get; set; }
        public string? Firstname { get; set; }
        public string? Lastname { get; set; }
        public int? Status { get; set; }
        public int? Gender { get; set; }
        public string? Phone { get; set; }
        public int? TotalOrders { get; set; }
        public decimal? TotalRevenue { get; set; }
        public decimal? TotalMoney { get; set; }
        public decimal? TotalPurchased { get; set; }
        public decimal? TotalCommission { get; set; }
        public string? Email { get; set; }
        public string? AddressDetail { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public int? UserRole { get; set; }
        public bool? IsVerified { get; set; }
        public string? AffiliateLevel { get; set; }
        public string? MyRef { get; set; }
        public string? UserType { get; set; }
        public bool IsUserAppRaw { get; set; }
        public string? ConnectionId { get; set; }
        public bool IsOnline { get; set; }
    }
}
