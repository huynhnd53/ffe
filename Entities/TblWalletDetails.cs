﻿using System.ComponentModel.DataAnnotations.Schema;

namespace FFE.Entities
{
    [Table("TblWalletDetails")]
    public class TblWalletDetails : IAuditableEntity
    {
        public long? UserId { get; set; }
        public string? Username { get; set; }
        public long? WalletId { get; set; }
        public decimal? Balance { get; set; }
        public decimal? BalanceAvaiable { get; set; }
        public decimal? Price { get; set; }
        public long? TokenId { get; set; }
        public string? TokenName { get; set; }
        public string? TokenCode { get; set; }
        //địa chỉ ví của user 
        public string? TokenAddressBep20 { get; set; }
        //địa chỉ ví của user
        public string? TokenAddressERC20 { get; set; }
    }
}
