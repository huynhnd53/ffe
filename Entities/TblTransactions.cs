﻿using System.ComponentModel.DataAnnotations.Schema;

namespace FFE.Entities
{
    [Table("TblTransactions")]
    public class TblTransactions : IAuditableEntity
    {
        public long? TransactionSource { get; set; }
        public string? TransactionSourceUsername { get; set; }
        public long? TransactionTarget { get; set; }
        public string? TransactionTargetUsername { get; set; }
        public int? TransactionType { get; set; }
        public int? Status { get; set; }
        public decimal? Amount { get; set; }
        public long? TokenId { get; set; }
        public string? TokenCode { get; set; }
        public string? Descriptions { get; set; }
    }
}
