﻿using System.ComponentModel.DataAnnotations.Schema;

namespace FFE.Entities
{
    [Table("TblCategories")]
    public class TblCategories : IAuditableEntity
    {
        public string? CategoryName { get; set; }
        public string? Descriptions { get; set; }
        public int? Status { get; set; }
        public bool? IsShow { get; set; } = false;
    }
}
