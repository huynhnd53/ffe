﻿using Microsoft.EntityFrameworkCore;

namespace FFE.Entities
{
    public class FFEContext : DbContext
    {
        public FFEContext()
        {
        }

        public FFEContext(DbContextOptions<FFEContext> options)
            : base(options)
        {
        }
        public virtual DbSet<TblUsers> TblUsers { get; set; }
        public virtual DbSet<TblPosts> TblPosts { get; set; }
        public virtual DbSet<TblCategories> TblCategories { get; set; }
        public virtual DbSet<TblProducts> TblProducts { get; set; }
        public virtual DbSet<TblNotifications> TblNotifications { get; set; }
        public virtual DbSet<TblTransactions> TblTransactions { get; set; }
        public virtual DbSet<TblWalletDetails> TblWalletDetails { get; set; }
        public virtual DbSet<TblTokens> TblTokens { get; set; }
        public virtual DbSet<TblEarningActions> TblEarningActions { get; set; }
        public virtual DbSet<TblConfigAttendDailys> TblConfigAttendDailys { get; set; }
        public virtual DbSet<TblAttendedDailys> TblAttendedDailys { get; set; }
        public virtual DbSet<TblMesssage> TblMesssage { get; set; }
        


        public virtual DbSet<TblUsersAppRaw> TblUsersAppRaw { get; set; }
    }
}
