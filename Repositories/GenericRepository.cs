﻿using FFE.Entities;
using FFE.Repositories;

using Microsoft.EntityFrameworkCore;

using System.Linq.Expressions;

namespace FFE.Repositories
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class
    {
        private readonly FFEContext _dbContext;

        public GenericRepository(FFEContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IQueryable<TEntity> GetAll()
        {
            return _dbContext.Set<TEntity>().AsNoTracking();
        }
        public IQueryable<TEntity> GetWhere(Expression<Func<TEntity, bool>> predicate)
        {
            return _dbContext.Set<TEntity>().AsNoTracking().Where(predicate);
        }


        public async Task Create(TEntity entity)
        {
            _dbContext.Set<TEntity>().Add(entity);
            _dbContext.SaveChanges();
        }
        public async Task Update(TEntity entity)
        {
            _dbContext.Set<TEntity>().Update(entity);
            _dbContext.SaveChanges();
        }
        public async Task Delete(TEntity entity)
        {
            _dbContext.Set<TEntity>().Remove(entity);
            _dbContext.SaveChanges();
        }
        public async Task CreateRange(List<TEntity> entities)
        {
            _dbContext.Set<TEntity>().AddRange(entities);
            _dbContext.SaveChanges();
        }
        public async Task UpdateRange(List<TEntity> entities)
        {
            _dbContext.Set<TEntity>().UpdateRange(entities);
            _dbContext.SaveChanges();
        }
        public async Task DeleteRange(List<TEntity> entities)
        {
            _dbContext.RemoveRange(entities);
            _dbContext.SaveChanges();
        }

        private void UpdateEntity(List<TEntity> entities)
        {
            //var lstUpdate = new List<TEntity>();
            //if (entities != null)
            //{
            //    foreach (var entity in entities)
            //    {
            //        var existingEntity = _dbContext.Set<TEntity>().Local.FirstOrDefault(e => e.Id == entity.Id);
            //        if (existingEntity != null)
            //        {
            //            _dbContext.Entry(existingEntity).CurrentValues.SetValues(entity);
            //            lstUpdate.Add(existingEntity);
            //        }
            //        else lstUpdate.Add(entity);
            //    }
            //    if (lstUpdate.Any())
            //        _dbContext.Set<TEntity>().UpdateRange(lstUpdate);
            //}
        }


        #region async
        public async Task CreateAsync(TEntity entity)
        {
            await _dbContext.Set<TEntity>().AddAsync(entity);
            await _dbContext.SaveChangesAsync();
        }
        public async Task UpdateAsync(TEntity entity)
        {
            _dbContext.Set<TEntity>().Update(entity);
            await _dbContext.SaveChangesAsync();
        }
        public async Task DeleteAsync(TEntity entity)
        {
            _dbContext.Set<TEntity>().Remove(entity);
            await _dbContext.SaveChangesAsync();
        }
        public async Task CreateRangeAsync(List<TEntity> entities)
        {
            _dbContext.Set<TEntity>().AddRange(entities);
            await _dbContext.SaveChangesAsync();
        }
        public async Task UpdateRangeAsync(List<TEntity> entities)
        {
            _dbContext.Set<TEntity>().UpdateRange(entities);
            await _dbContext.SaveChangesAsync();
        }
        public async Task DeleteRangeAsync(List<TEntity> entities)
        {
            _dbContext.RemoveRange(entities);
            await _dbContext.SaveChangesAsync();
        }
        #endregion

        public async Task ExecuteSqlCommand(string query)
        {
            await _dbContext.Database.ExecuteSqlRawAsync(query);
        }
    }
}
