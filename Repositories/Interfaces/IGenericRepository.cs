﻿using System.Linq.Expressions;

namespace FFE.Repositories
{
    public interface IGenericRepository<TEntity> where TEntity : class
    {
        IQueryable<TEntity> GetAll();
        IQueryable<TEntity> GetWhere(Expression<Func<TEntity, bool>> predicate);
        Task Create(TEntity entity);
        Task Update(TEntity entity);
        Task Delete(TEntity entity);
        Task CreateAsync(TEntity entity);
        Task UpdateAsync(TEntity entity);
        Task DeleteAsync(TEntity entity);
        Task CreateRange(List<TEntity> entities);
        Task UpdateRange(List<TEntity> entities);
        Task DeleteRange(List<TEntity> entities);
        Task CreateRangeAsync(List<TEntity> entities);
        Task UpdateRangeAsync(List<TEntity> entities);
        Task DeleteRangeAsync(List<TEntity> entities);
        Task ExecuteSqlCommand(string query);
    }
}
