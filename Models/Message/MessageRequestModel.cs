﻿namespace FFE.Models.Message
{
    public class MessageRequestModel
    {
    }
    public class SaveMessageToUserRequestModel
    {
        public long UserTargetID { get; set; } = 0;
        public string UserTargetName { get; set; }
        public string MesssageContent { get; set; }
    }

    public class MessageToUserRequestModel : PagingRequestModel
    {
        public long UserTargetID { get; set; } = 0;
        public string UserTargetName { get; set; } = string.Empty;
    }
}
