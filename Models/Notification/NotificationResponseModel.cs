﻿namespace FFE.Models.Notification
{
    public class NotificationResponseModel
    {
        public long Id { set; get; }
        public long? UserId { get; set; }
        public string? Username { get; set; }
        public int? Status { get; set; }
        public string? Contents { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
