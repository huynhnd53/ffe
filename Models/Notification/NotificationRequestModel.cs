﻿namespace FFE.Models.Notification
{
    public class NotificationRequestModel : PagingRequestModel
    {
        public int Status { get; set; } = -1;
    }

    public class MaskReadNotiRequestModel
    {
        public long NotificationId { get; set; }
    }
}
