﻿namespace FFE.Models.Posts
{
    public class PostResponseModel
    {
        public long Id { set; get; }
        public string? Title { get; set; }
        public string? Summary { get; set; }
        public string? Contents { get; set; }
        public int? Status { get; set; }
        public int? LikedCount { get; set; }
        public int? SharedCount { get; set; }
        public int? CommentedCount { get; set; }
        public string? Thumbnail { get; set; }
        public List<string>? Images { get; set; }
        public long? CategoriId { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}
