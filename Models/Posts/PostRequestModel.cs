﻿namespace FFE.Models.Posts
{
    public class PostRequestModel : PagingRequestModel
    {
        public string? TitleSearch { get; set; }
        public int Status { get; set; } = -1;
    }
}
