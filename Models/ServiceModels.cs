﻿using FFE.Common;

using System.Net;

using static FFE.Common.EnumBase;

namespace FFE.Models
{
    public class ResponseModel<T>
    {
        public int ResponseCode { get; set; } = (int)HttpStatusCode.BadRequest;
        public int Result { get; set; } = (int)ResponseResult.Error;
        public string? Message { get; set; } = "Có lỗi , vui lòng liên hệ IT";
        public T Data { get; set; }

        public void SetSuccess()
        {
            ResponseCode = (int)HttpStatusCode.OK;
            Result = (int)ResponseResult.Success;
            Message = MessageConstant.SUCCESS_SYSTEM;
        }
    }

    public class RequestHeaders
    {
        public string Name { set; get; }
        //public object Value { set; get; }
        public string Value { set; get; }
    }
    public class LoginappffeRequestModel
    {
        public string email { set; get; }
        public string password { set; get; }
    }

    public class appffeResponseModel<T>
    {
        public bool status { set; get; }
        public int total { get; set; } 
        public T users { get; set; }
    }

    public class appffeDataResponseModel<T>
    {
        public bool status { set; get; }
        public string message { get; set; }
        public T data { get; set; }
    }

    public class appffeLoginResponseModel<T>
    {
        public bool status { set; get; }
        public string message { set; get; }
        public string accessToken { set; get; }
        public string refreshToken { set; get; }
        public T user { get; set; }
    }

    public class appffeSignUpResponseModel
    {
        public bool status { set; get; }
        public string message { set; get; }
    }

    public class appffeConfirmOTPResponseModel
    {
        public bool status { set; get; } = false;
        public string message { set; get; } = "Có lỗi xảy ra, vui lòng thử lại";
        public string accessToken { set; get; }
        public string refreshToken { set; get; }
    }
}
