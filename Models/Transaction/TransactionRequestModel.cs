﻿using FFE.Entities;

using static FFE.Common.EnumBase;

namespace FFE.Models.Transaction
{
    public class TransactionRequestModel : PagingRequestModel
    {
        /// <summary>
        ///ReceiveFromSystem = 1, [Description("Thưởng từ hệ thống")]
        ///SendToken = 2, [Description("Chuyển token đi")]
        ///ReceiveToken = 3, [Description("Nhận token đi")]
        ///AttendedDaily = 4, [Description("Điểm danh")]
        /// </summary>
        public int TransactionType { get; set; } = -1;
        public int Status { get; set; } = -1;
    }

    public class SearchUsersTransactionRequestModel : PagingRequestModel
    {
        public string Name { get; set; } = "";
    }

    public class SendTokenTransactionRequestModel
    {
        public long? ReceiverId { get; set; }
        public long? TokenId { get; set; }
        public decimal? Amount { get; set; }
    }

    public class ClaimTokenEarnedRequestModel
    {
        public decimal Amount { get; set; }
    }

    public class UserWalletDetailModel
    {
        public string? Username { get; set; }
        public long? UserId { get; set; }
        public List<TblWalletDetails>? WalletDetails { get; set; }
    }

    public class WalletDetailModel
    {
        public long? WalletDetailId { get; set; }
        public long? TokenId { get; set; }
        public string? TokenCode { get; set; }
        public decimal? BalanceAvaiable { get; set; }
        public decimal? Balance { get; set; }
    }
}
