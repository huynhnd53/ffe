﻿namespace FFE.Models.Transaction
{
    public class TransactionResponseModel
    {
        public long Id { set; get; }
        public long? TransactionSource { get; set; }
        public long? TransactionTarget { get; set; }
        public int? TransactionType { get; set; }
        public decimal? Amount { get; set; }
        public long? TokenId { get; set; }
        public string? TokenCode { get; set; }
        public string? Descriptions { get; set; }
        public DateTime CreatedDate { get; set; }
    }

    public class SearchUsersTransactionResponseModel
    {
        public long? UserId { set; get; }
        public string? Username { get; set; }
        public string? Fullname { get; set; }
    }
}
