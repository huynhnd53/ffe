﻿using System.ComponentModel;

namespace FFE.Models.UsersModels
{
    public class UsersRequestModel
    {
    }

    public class SaveStatusConnectRequestModel
    {
        public string Username { get; set; } = string.Empty;
        public string ConnectionId { get; set; } = string.Empty;
        /// <summary>
        ///Online = 1, | [Description("Hoạt động")]
        ///Offline = 0 | [Description("Tắt Hoạt động")]
        /// </summary>
        public int StatusConnect { get; set; } = 0;
    }

    public class UsersOnlineRequestModel : PagingRequestModel
    {
        public string? Fullname { get; set; } = string.Empty;
    }
}
