﻿using FFE.Models.Wallet;

namespace FFE.Models
{
    public class UsersResponseModel
    {
        public string? Token { get; set; }
        public string? TokenAppRaw { get; set; }
        public string? RefreshTokenAppRaw { get; set; }
        public string? Username { get; set; }
        public string? IdAppRaw { get; set; }
        public string? Email { get; set; }
        public string? Fullname { get; set; }
        public int? Status { get; set; }
        public int? Gender { get; set; }
        public string? Phone { get; set; }
        public int? TotalOrders { get; set; }
        public decimal? TotalRevenue { get; set; }
        public decimal? TotalMoney { get; set; }
        public decimal? TotalPurchased { get; set; }
        public decimal? TotalCommission { get; set; }
        public string? AddressDetail { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public int? UserRole { get; set; }
        public bool? IsVerified { get; set; }
        public string? AffiliateLevel { get; set; }
        public string? MyRef { get; set; }
        public string? UserType { get; set; }
        public bool? IsUserAppRaw { get; set; }
        public int TotalMember { get; set; } = 0;

        public WalletResponseModel Wallet { get; set; }

    }

    public class UsersOnlineResponseModel
    {
        public string Username { get; set; } = string.Empty;
        public string Fullname { get; set; } = string.Empty;
        public bool IsOnline { get; set; } = false;
        public string ConnectionId { get; set; } = string.Empty;
    }

    public class MessageToUserResponseModel
    {
        public long UserSourceID { get; set; } = 0;
        public string UserSourceName { get; set; }
        public long UserTargetID { get; set; } = 0;
        public string UserTargetName { get; set; }
        public string MesssageContent { get; set; }
        public bool IsRead { get; set; } = false;
    }
}
