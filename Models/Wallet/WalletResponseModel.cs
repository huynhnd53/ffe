﻿namespace FFE.Models.Wallet
{
    public class WalletResponseModel
    {
        public decimal? Balance { get; set; } = 0;
        public decimal? BalanceAvaiable { get; set; } = 0;
        public List<WalletDetailResponseModel>? WalletDetails { get; set; }
    }
    public class WalletDetailResponseModel
    {
        public decimal? Balance { get; set; } = 0;
        public decimal? BalanceAvaiable { get; set; } = 0;
        public decimal? Price { get; set; } = 0;
        public long? TokenId { get; set; } = 0;
        public string? TokenName { get; set; } = string.Empty;
        public string? TokenCode { get; set; } = string.Empty;
    }
}
