﻿using Azure.Messaging;

using FFE.Common;

using static FFE.Common.EnumBase;

namespace FFE.Models
{
    public class PagingResponseModel<T>
    {
        public int Result { get; set; } = (int)ResponseResult.Error;
        public string? Message { get; set; } = "Có lỗi , vui lòng liên hệ IT";
        public int PageIndex { get; set; } = 1;
        public int PageSize { get; set; } = 10;
        public int TotalCount { get; set; }
        public int TotalPages => (int)Math.Ceiling((double)TotalCount / PageSize);
        public List<T>? Data { get; set; }

        public void SetSuccess()
        {
            Result = (int)ResponseResult.Success;
            Message = MessageConstant.SUCCESS_SYSTEM;
        }
    }

    public class PagingRequestModel
    {
        public int PageIndex { get; set; } = 1;
        public int PageSize { get; set; } = 10;
    }
   
}
