﻿namespace FFE.Models.ServicesAPIModels
{
    public class FFEAppUsersModel
    {
        public string? _id { get; set; }
        public string? email { get; set; }
        public string? password { get; set; }
        public bool? isVerified { get; set; }
        public string? fullName { get; set; }
        public string? affiliateLevel { get; set; }
        public string? type { get; set; }
        public bool? isChangePassWord { get; set; }
        public bool? isLock { get; set; }
        public bool? isDelete { get; set; }
        public string? myRef { get; set; }
        public long? totalOrder { get; set; }
        public long? totalRevenue { get; set; }
        public long? totalMoney { get; set; }
        public long? totalPurchase { get; set; }
        public long? totalCommission { get; set; }
        public DateTime? lastLogin { get; set; }
        public DateTime? createdAt { get; set; }
        public DateTime? updatedAt { get; set; }
        public DateTime? birthDay { get; set; }
        public string? phoneNumber { get; set; }
        public long? __v { get; set; }
        public string? parentID { get; set; }
    }


    public class FFEAppUsersLoginResponseModel
    {
        public string _id { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public bool isVerified { get; set; }
        public string fullName { get; set; }
        public string affiliateLevel { get; set; }
        public string type { get; set; }
        public bool isChangePassWord { get; set; }
        public bool isLock { get; set; }
        public bool isDelete { get; set; }
        public string myRef { get; set; }
        public long totalOrder { get; set; }
        public long totalRevenue { get; set; }
        public long totalMoney { get; set; }
        public long totalPurchase { get; set; }
        public long totalCommission { get; set; }
        public DateTime? lastLogin { get; set; }
        public DateTime? createdAt { get; set; }
        public DateTime? updatedAt { get; set; }
        public DateTime? birthDay { get; set; }
        public string phoneNumber { get; set; }
        public long __v { get; set; }
        public long totalVocher { get; set; }
        public string parentID { get; set; }
        public WalletInfoLoginResponseModel walletInfo { get; set; }
        public ProfitWalletInfoLoginResponseModel profitWalletInfo { get; set; }
        public ParentInfoLoginResponseModel parentInfo { get; set; }
    }

    public class WalletInfoLoginResponseModel
    {
        public string _id { get; set; }
        public string userID { get; set; }
        public long pointVIC { get; set; }
        public long numberDailyRollCall { get; set; }
        public long accountBalance { get; set; }
        public long amountIsPending { get; set; }
        public long amountVoucher { get; set; }
        public long amountShare { get; set; }
        public long totalAccumulated { get; set; }
        public long totalAccumulatedMoneyOfSubordinates { get; set; }
        //public List<string> bankAccounts { get; set; }
        public DateTime? createdAt { get; set; }
        public DateTime? updatedAt { get; set; }
        public long walletID { get; set; }
        public long __v { get; set; }
        public DateTime? lastDailyRollCall { get; set; }
        public long totalCoin { get; set; }
    }
    public class ProfitWalletInfoLoginResponseModel
    {
        public string _id { get; set; }
        public string userID { get; set; }
        public long total { get; set; }
        public long totalWithdrawn { get; set; }
        public long totalBalance { get; set; }
        public DateTime? createdAt { get; set; }
        public DateTime? updatedAt { get; set; }
        public long ProfitWalletID { get; set; }
        public long __v { get; set; }
    }
    public class ParentInfoLoginResponseModel
    {
        public string _id { get; set; }
        public string email { get; set; }
        public string fullName { get; set; }
    }
}
