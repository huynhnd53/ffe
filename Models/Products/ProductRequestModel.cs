﻿namespace FFE.Models.Posts
{
    public class ProductRequestModel : PagingRequestModel
    {
        public string? ProductNameSearch { get; set; }
        public int Status { get; set; } = -1;
    }
}
