﻿namespace FFE.Models.ConfigAttendDailys
{
    public class ConfigAttendDailyResponseModel
    {
        public long Id { set; get; }
        public DateTime AttendDate { get; set; }
        public int ConfigDay { get; set; } = 0;
        public int ConfigMonth { get; set; } = 0;
        public int ConfigYear { get; set; } = 0;
        public decimal? Points { get; set; }
    }

    
}
