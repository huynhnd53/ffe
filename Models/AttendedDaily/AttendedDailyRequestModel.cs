﻿namespace FFE.Models.AttendedDaily
{
    public class AttendedDailyRequestModel
    {
        public int Day { get; set; } = 0;
        public int Month { get; set; } = 0;
        public int Year { get; set; } = 0;
    }
}
