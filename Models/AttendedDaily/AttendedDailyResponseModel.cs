﻿namespace FFE.Models.AttendedDaily
{
    public class AttendedDailyResponseModel
    {

        public class UserAttendDailyResponseModel
        {
            public long UserId { set; get; }
            public DateTime AttendDate { get; set; }
            public int ConfigDay { get; set; } = 0;
            public int ConfigMonth { get; set; } = 0;
            public int ConfigYear { get; set; } = 0;
            public decimal? Points { get; set; }
        }

        public class UserAttendDailyReportResponseModel
        {
            public long UserId { set; get; }
            public long ConfigId { set; get; }
            public bool IsAttended { set; get; } = false;
            public DateTime AttendDate { get; set; }
            public int ConfigDay { get; set; } = 0;
            public int ConfigMonth { get; set; } = 0;
            public int ConfigYear { get; set; } = 0;
            public decimal Points { get; set; } = 0;
        }

    }
}
