﻿namespace FFE.Models.AuthenticationModels
{
    public class LoginRequestModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }

    public class RefCodeRequestModel
    {
        public string refCode { get; set; }
    }

    public class ConfirmOTPRequestModel
    {
        public string email { get; set; }
        public string code { get; set; }
    }

    public class SignUpRequestModel
    {
        public string fullname { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public string repeatPassword { get; set; }
        public string refCode { get; set; }
    }

    public class VerifyEmailRequestModel
    {
        public string email { get; set; }
    }

    public class SetNewPasswordRequestModel
    {
        public string accessToken { get; set; }
        public string newPassword { get; set; }
        public string repeatNewPassword { get; set; }
    }

    public class SetNewPasswordModel
    {
        public string newPassword { get; set; }
        public string repeatNewPassword { get; set; }
    }
}
