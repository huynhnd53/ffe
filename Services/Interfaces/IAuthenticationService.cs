﻿using FFE.Models;
using FFE.Models.AuthenticationModels;

namespace FFE.Services
{
    public interface IAuthenticationService
    {
        Task<ResponseModel<UsersResponseModel>> Login(LoginRequestModel request);
        Task<ResponseModel<bool>> IsExistedRefCode(RefCodeRequestModel request);
        Task<ResponseModel<bool>> SignUp(SignUpRequestModel request);
        Task<ResponseModel<bool>> ConfirmOTPMailSignUp(ConfirmOTPRequestModel request);
        Task<ResponseModel<bool>> ForgetPassword(VerifyEmailRequestModel request);
        Task<ResponseModel<bool>> VerifyEmail(VerifyEmailRequestModel request);
        Task<ResponseModel<bool>> SetNewPassword(SetNewPasswordRequestModel request);
        Task<appffeConfirmOTPResponseModel> ConfirmOTPForgotPassword(ConfirmOTPRequestModel request);
        Task<ResponseModel<bool>> SyncUserAppRaw(string? passcode);
        Task<ResponseModel<UsersResponseModel>> UserInfo(string username, string accessToken);
        Task SaveConnectChat(string connectionId);
    }
}
