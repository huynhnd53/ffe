﻿using FFE.Models.ServicesAPIModels;
using FFE.Models;
using FFE.Models.AuthenticationModels;

namespace FFE.Services.Interfaces
{
    public interface IServiceAPI
    {
        Task<appffeResponseModel<List<FFEAppUsersModel>>> GetListUsers();
        Task<appffeLoginResponseModel<FFEAppUsersLoginResponseModel>> LoginViaFfeAppRaw(LoginappffeRequestModel request);
        Task<appffeSignUpResponseModel> SignUp(SignUpRequestModel request);
        Task<appffeSignUpResponseModel> ConfirmOTPMailSignUp(ConfirmOTPRequestModel request);
        Task<appffeSignUpResponseModel> RequestConfirmEmail(VerifyEmailRequestModel request);
        Task<appffeSignUpResponseModel> RequestSetNewPassword(SetNewPasswordModel request, string accessToken);
        Task<appffeConfirmOTPResponseModel> ConfirmOTPForgotPassRequest(ConfirmOTPRequestModel request);
        Task<appffeDataResponseModel<FFEAppUsersLoginResponseModel>> UsersProfile(string accessToken);
    }
}
