﻿using FFE.Entities;
using FFE.Models;
using FFE.Models.ConfigAttendDailys;

using Microsoft.EntityFrameworkCore;

using System.Collections.Generic;

namespace FFE.Services
{
    public interface IConfigAttendDailyService
    {
        Task<ResponseModel<List<TblConfigAttendDailys>>> GetConfigAttendThisMonth();
        Task<ResponseModel<List<TblConfigAttendDailys>>> GetConfigAttendByMonth(int month, int year);
        List<ConfigAttendDailyResponseModel> GetConfigMonth(int month, int year);

    }

    public class ConfigAttendDailyService : IConfigAttendDailyService
    {
        FFEContext _dbContext;
        private readonly IAttendedDailyService _attendedDailyService;
        public ConfigAttendDailyService(FFEContext dbContext)
        {
            _dbContext = dbContext;
        }

        public Task<ResponseModel<List<TblConfigAttendDailys>>> GetConfigAttendByMonth(int month, int year)
        {
            throw new NotImplementedException();
        }

        public Task<ResponseModel<List<TblConfigAttendDailys>>> GetConfigAttendThisMonth()
        {
            throw new NotImplementedException();
        }

        public List<ConfigAttendDailyResponseModel> GetConfigMonth(int month, int year)
        {
            return _dbContext.TblConfigAttendDailys.Where(x => x.ConfigMonth == month && x.ConfigYear == year).Select(x => new ConfigAttendDailyResponseModel
            {
                Id = x.Id,
                AttendDate = x.AttendDate,
                ConfigDay = x.ConfigDay,
                ConfigMonth = x.ConfigMonth,
                ConfigYear = x.ConfigYear,
                Points = x.Points
            }).ToList();
        }

    }
}
