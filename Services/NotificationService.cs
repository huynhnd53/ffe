﻿using FFE.Models.Posts;
using FFE.Models;
using FFE.Entities;
using FFE.Models.Notification;
using static FFE.Common.EnumBase;
using FFE.Models.Transaction;
using Azure.Core;
using Microsoft.EntityFrameworkCore;

namespace FFE.Services
{
    public interface INotificationService
    {
        Task<PagingResponseModel<NotificationResponseModel>> GetPagingMyNoti(NotificationRequestModel request, string username);
        Task<ResponseModel<bool>> MaskReadNoti(long id, string username);
        Task<ResponseModel<bool>> MaskReadAllNoti(string username);

    }

    public class NotificationService : INotificationService
    {
        FFEContext _dbContext;
        public NotificationService(FFEContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<PagingResponseModel<NotificationResponseModel>> GetPagingMyNoti(NotificationRequestModel request, string username)
        {
            PagingResponseModel<NotificationResponseModel> response = new()
            {
                PageIndex = request.PageIndex,
                PageSize = request.PageSize,
            };
            try
            {
                List<NotificationResponseModel> lstDataResponse = new();
                var notis = _dbContext.TblNotifications.Where(x => !string.IsNullOrEmpty(x.Username)
                                                            && x.Username.ToLower().Trim().Equals(username.ToLower().Trim())
                                                            && (request.Status == -1 || x.Status == request.Status))
                                                .AsQueryable();

                response.TotalCount = notis.Count();
                notis = notis.OrderByDescending(x => x.CreatedDate).Skip((request.PageIndex - 1) * request.PageSize).Take(request.PageSize).AsQueryable();
                foreach (var item in notis)
                {
                    NotificationResponseModel itemResponse = new();
                    #region map model
                    itemResponse.Id = item.Id;
                    itemResponse.UserId = item.UserId;
                    itemResponse.Username = item.Username;
                    itemResponse.Status = item.Status;
                    itemResponse.Contents = item.Contents;
                    itemResponse.CreatedDate = item.CreatedDate;
                    #endregion
                    lstDataResponse.Add(itemResponse);
                }
                response.Data = lstDataResponse;
                response.SetSuccess();
            }
            catch (Exception)
            {
                throw;
            }
            return response;
        }

        public async Task<ResponseModel<bool>> MaskReadAllNoti(string username)
        {
            ResponseModel<bool> response = new();
            try
            {
                var notis = _dbContext.TblNotifications.Where(x => !string.IsNullOrEmpty(x.Username)
                                                           && x.Username.ToLower().Trim().Equals(username.ToLower().Trim()))
                                                        .ToList();
                if (notis == null || notis.Count == 0)
                    return response;

                foreach (var noti in notis)
                {
                    noti.Status = 2;
                }

                 _dbContext.TblNotifications.UpdateRange(notis);
                await _dbContext.SaveChangesAsync();
                response.Data = true;
                response.SetSuccess();
            }
            catch (Exception)
            {
                throw;
            }
            return response;
        }

        public async Task<ResponseModel<bool>> MaskReadNoti(long id, string username)
        {
            ResponseModel<bool> response = new();
            try
            {
                var noti = _dbContext.TblNotifications.Where(x => x.Id == id && !string.IsNullOrEmpty(x.Username)
                                                           && x.Username.ToLower().Trim().Equals(username.ToLower().Trim()))
                                                        .FirstOrDefault();
                if(noti == null)
                    return response;

                _dbContext.TblNotifications.Update(noti);
                await _dbContext.SaveChangesAsync();
                response.Data = true;
                response.SetSuccess();
            }
            catch (Exception)
            {
                throw;
            }
            return response;
        }
    }
}
