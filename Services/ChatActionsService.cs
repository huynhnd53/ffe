﻿using FFE.Entities;
using FFE.Models.Transaction;
using FFE.Models;
using Microsoft.EntityFrameworkCore;
using FFE.Models.UsersModels;
using static FFE.Common.EnumBase;
using FFE.Models.Message;

namespace FFE.Services
{
    public interface IChatActionsService
    {
        Task<ResponseModel<bool>> SaveChangeStatus(SaveStatusConnectRequestModel request);
        Task<ResponseModel<bool>> SaveMessengerToUser(SaveMessageToUserRequestModel request, string username, long userId);
        Task<PagingResponseModel<UsersOnlineResponseModel>> GetPagingUserOnline(UsersOnlineRequestModel request, string username);
        Task<PagingResponseModel<MessageToUserResponseModel>> GetPagingMessageToUser(MessageToUserRequestModel request, string username, long userId);
        Task<string> GetWelcomeMessage();
    }

    public class ChatActionsService : IChatActionsService
    {
        FFEContext _dbContext;
        public ChatActionsService(FFEContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<ResponseModel<bool>> SaveChangeStatus(SaveStatusConnectRequestModel request)
        {
            ResponseModel<bool> response = new();
            try
            {
                if (string.IsNullOrEmpty(request.Username))
                {
                    response.Message = "Usernam cannot null or empty.";
                    return response;
                }
                if (!Enum.IsDefined(typeof(EnumIsOnline), request.StatusConnect))
                {
                    response.Message = "StatusConnect not correct.";
                    return response;
                }
                else if (request.StatusConnect == (int)EnumIsOnline.Online && string.IsNullOrEmpty(request.ConnectionId))
                {
                    response.Message = "ConnectionId cannot null or empty.";
                    return response;
                }
                var user = await _dbContext.TblUsers.Where(x => x.Username.ToLower().Equals(request.Username.ToLower())).FirstOrDefaultAsync();
                if (user == null)
                {
                    response.Message = "Usernam not exist.";
                    return response;
                }
                user.ConnectionId = request.ConnectionId;
                user.IsOnline = request.StatusConnect == (int)EnumIsOnline.Online;
                user.UpdatedDate = DateTime.Now;
                _dbContext.TblUsers.Update(user);
                await _dbContext.SaveChangesAsync();
                response.SetSuccess();
                response.Data = true;
            }
            catch (Exception ex)
            {
            }
            return response;
        }
        public async Task<ResponseModel<bool>> SaveMessengerToUser(SaveMessageToUserRequestModel request, string username, long userId)
        {
            ResponseModel<bool> response = new();
            try
            {
                if (string.IsNullOrEmpty(request.UserTargetName) || string.IsNullOrEmpty(request.MesssageContent) || request.UserTargetID == 0)
                {
                    response.Message = "Please fill full input";
                    return response;
                }
                if (!await _dbContext.TblUsers.AnyAsync(x => x.Username == request.UserTargetName && x.Id == request.UserTargetID))
                {
                    response.Message = "UserTarget not exist";
                    return response;
                }
                _dbContext.TblMesssage.Add(new TblMesssage
                {
                    UserSourceId = userId,
                    UserSourceName = username,
                    UserTargetID = request.UserTargetID,
                    UserTargetName = request.UserTargetName,
                    MesssageContent = request.MesssageContent,
                });
                await _dbContext.SaveChangesAsync();
                response.SetSuccess();
                response.Data = true;
            }
            catch (Exception ex)
            {
            }
            return response;
        }
        public async Task<PagingResponseModel<UsersOnlineResponseModel>> GetPagingUserOnline(UsersOnlineRequestModel request, string username)
        {
            PagingResponseModel<UsersOnlineResponseModel> response = new()
            {
                PageIndex = request.PageIndex,
                PageSize = request.PageSize,
            };
            List<UsersOnlineResponseModel> lstDataResponse = new();
            try
            {
                var users = _dbContext.TblUsers.Where(x => !string.IsNullOrEmpty(x.Fullname) && x.Username != username).AsQueryable();
                if (!string.IsNullOrEmpty(request.Fullname))
                    users = users.Where(x => x.Fullname.ToLower().Contains(request.Fullname.ToLower()));

                response.TotalCount = users.Count();
                users = users.OrderByDescending(x => x.IsOnline).ThenBy(x=>x.UpdatedDate)
                    .Skip((request.PageIndex - 1) * request.PageSize).Take(request.PageSize).AsQueryable();

                foreach(var  user in users)
                {
                    lstDataResponse.Add(new UsersOnlineResponseModel
                    {
                        ConnectionId = user.ConnectionId ?? "", 
                        Fullname = user.Fullname??"", 
                        IsOnline = user.IsOnline,
                        Username = user.Username ?? ""
                    });
                }
                response.Data = lstDataResponse;
                response.SetSuccess();
            }
            catch (Exception ex)
            {
            }
            return response;
        }
        public async Task<PagingResponseModel<MessageToUserResponseModel>> GetPagingMessageToUser(MessageToUserRequestModel request, string username, long userId)
        {
            PagingResponseModel<MessageToUserResponseModel> response = new()
            {
                PageIndex = request.PageIndex,
                PageSize = request.PageSize,
            };
            List<MessageToUserResponseModel> lstDataResponse = new();
            try
            {
                var messages = _dbContext.TblMesssage.Where(x => (x.UserSourceId == userId || x.UserSourceId == request.UserTargetID) 
                                                                && (x.UserTargetID == userId || x.UserTargetID == request.UserTargetID))
                                                    .AsQueryable();
                response.TotalCount = messages.Count();
                messages = messages.OrderByDescending(x => x.CreatedDate).Skip((request.PageIndex - 1) * request.PageSize).Take(request.PageSize).AsQueryable();
                foreach(var m in messages)
                {
                    lstDataResponse.Add(new MessageToUserResponseModel
                    {
                        UserSourceID = m.UserSourceId, 
                        UserSourceName = m.UserSourceName, 
                        UserTargetID = m.UserTargetID,
                        UserTargetName = m.UserTargetName,
                        MesssageContent = m.MesssageContent,
                        IsRead = m.IsRead
                    });
                }
                response.Data = lstDataResponse;
                response.SetSuccess();
            }
            catch (Exception ex)
            {
            }
            return response;
        }
       
        public async Task<string> GetWelcomeMessage()
        {
            try
            {
                // Your logic to get the welcome message
                return "Welcome to the chat!";
            }
            catch (Exception ex)
            {
                // Log the exception for debugging
                // You can also return a default message or rethrow the exception if needed.
                // For troubleshooting purposes, log the exception details to get more insights.
                Console.WriteLine($"Error in GetWelcomeMessage: {ex.Message}");
                throw; // Rethrow the exception if required.
            }
        }
    }
}
