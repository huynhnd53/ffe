﻿using Azure;
using Azure.Core;

using FFE.Models;
using FFE.Models.AuthenticationModels;
using FFE.Models.ServicesAPIModels;
using FFE.Services;
using FFE.Services.Interfaces;

using Newtonsoft.Json;

using System.Globalization;
using System.Net.Http.Headers;
using System.Text;

namespace N2EC_Report.Services
{
    public class ServiceAPI : MainService, IServiceAPI
    {
        private IConfiguration _config;
        public ServiceAPI(IConfiguration config)
        {
            _config = config;
        }
        public async Task<appffeResponseModel<List<FFEAppUsersModel>>> GetListUsers()
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Add("api-key", _config["ffeapikey"]);
            var response = await client.PostAsync("https://app.ffe.vn/api/private/list-user", null);
            var responseString = await response.Content.ReadAsStringAsync();
            var lstFFEAppUsers = JsonConvert.DeserializeObject<appffeResponseModel<List<FFEAppUsersModel>>>(responseString);
            return lstFFEAppUsers ?? new appffeResponseModel<List<FFEAppUsersModel>>();
        }

        public async Task<appffeLoginResponseModel<FFEAppUsersLoginResponseModel>> LoginViaFfeAppRaw(LoginappffeRequestModel request)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Add("api-key", _config["ffeapikey"]);
            var content = new StringContent(JsonConvert.SerializeObject(request), Encoding.UTF8, "application/json");
            var response = await client.PostAsync("https://app.ffe.vn/api/user/login", content);
            var responseString = await response.Content.ReadAsStringAsync();
            var lstFFEAppUsers = JsonConvert.DeserializeObject<appffeLoginResponseModel<FFEAppUsersLoginResponseModel>>(responseString);
            return lstFFEAppUsers ?? new appffeLoginResponseModel<FFEAppUsersLoginResponseModel>();
        }

        public async Task<appffeSignUpResponseModel> SignUp(SignUpRequestModel request)
        {
            var client = new HttpClient();
            var content = new StringContent(JsonConvert.SerializeObject(request), Encoding.UTF8, "application/json");
            var response = await client.PostAsync("https://app.ffe.vn/api/user/sign-in", content);
            var responseString = await response.Content.ReadAsStringAsync();
            var lstFFEAppUsers = JsonConvert.DeserializeObject<appffeSignUpResponseModel>(responseString);
            return lstFFEAppUsers ?? new appffeSignUpResponseModel();
        }
        public async Task<appffeSignUpResponseModel> ConfirmOTPMailSignUp(ConfirmOTPRequestModel request)
        {
            var client = new HttpClient();
            var content = new StringContent(JsonConvert.SerializeObject(request), Encoding.UTF8, "application/json");
            var response = await client.PostAsync("https://app.ffe.vn/api/user/verify-email-sign-in", content);
            var responseString = await response.Content.ReadAsStringAsync();
            var lstFFEAppUsers = JsonConvert.DeserializeObject<appffeSignUpResponseModel>(responseString);
            return lstFFEAppUsers ?? new appffeSignUpResponseModel();
        }
        public async Task<appffeSignUpResponseModel> RequestConfirmEmail(VerifyEmailRequestModel request)
        {
            var client = new HttpClient();
            var content = new StringContent(JsonConvert.SerializeObject(request), Encoding.UTF8, "application/json");
            var response = await client.PostAsync("https://app.ffe.vn/api/user/verify-email", content);
            var responseString = await response.Content.ReadAsStringAsync();
            var lstFFEAppUsers = JsonConvert.DeserializeObject<appffeSignUpResponseModel>(responseString);
            return lstFFEAppUsers ?? new appffeSignUpResponseModel();
        }
        public async Task<appffeConfirmOTPResponseModel> ConfirmOTPForgotPassRequest(ConfirmOTPRequestModel request)
        {
            var client = new HttpClient();
            var content = new StringContent(JsonConvert.SerializeObject(request), Encoding.UTF8, "application/json");
            var response = await client.PostAsync("https://app.ffe.vn/api/user/confirm-otp", content);
            var responseString = await response.Content.ReadAsStringAsync();
            var lstFFEAppUsers = JsonConvert.DeserializeObject<appffeConfirmOTPResponseModel>(responseString);
            return lstFFEAppUsers ?? new appffeConfirmOTPResponseModel();
        }
        public async Task<appffeSignUpResponseModel> RequestSetNewPassword(SetNewPasswordModel request, string accessToken)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            var content = new StringContent(JsonConvert.SerializeObject(request), Encoding.UTF8, "application/json");
            var response = await client.PostAsync("https://app.ffe.vn/api/user/change-password", content);
            var responseString = await response.Content.ReadAsStringAsync();
            var lstFFEAppUsers = JsonConvert.DeserializeObject<appffeSignUpResponseModel>(responseString);
            return lstFFEAppUsers ?? new appffeSignUpResponseModel();
        }

        public async Task<appffeDataResponseModel<FFEAppUsersLoginResponseModel>> UsersProfile(string accessToken)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            var response = await client.GetAsync("https://app.ffe.vn/api/user/profile-detail");
            var responseString = await response.Content.ReadAsStringAsync();
            var lstFFEAppUsers = JsonConvert.DeserializeObject<appffeDataResponseModel<FFEAppUsersLoginResponseModel>>(responseString);
            return lstFFEAppUsers ?? new appffeDataResponseModel<FFEAppUsersLoginResponseModel>();
        }
    }
}
