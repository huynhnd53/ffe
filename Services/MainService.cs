﻿using AutoMapper;

using FFE.Models;

using N2EC_Report.Common;

using Newtonsoft.Json;
using System.Globalization;
using System.Net;
using System.Reflection.PortableExecutable;
using System.Text;


namespace FFE.Services
{
    public class MainService
    {
        public ILogger _logger;
        public IMapper _mapper;
        public IConfiguration _config;

        public string BaseUri = "";
        public string AuthValue = "";

        public enum HttpMethod
        {
            POST,
            GET,
            PUT,
            DELETE
        }
        public void ConvertDateTime(string input, out DateTime output, bool startDate = true)
        {
            if (!string.IsNullOrEmpty(input))
            {
                output = FromStringToTime(input);
                if (!startDate)
                {
                    output = output.AddHours(24);
                }
            }
            else
            {
                if (startDate)
                {
                    output = DateTime.Now.Date.AddDays(-7);
                }
                else
                {
                    output = DateTime.Now.Date.AddHours(24);
                }
            }
        }

        protected static DateTime FromStringToTime(string input)
        {
            if (input.Length < 12)
            {
                input += " 00:00:00";
            }

            try
            {
                return DateTime.ParseExact(input, "dd/M/yyyy HH:mm:ss",
                    CultureInfo.InvariantCulture);
            }
            catch (Exception)
            {
                try
                {
                    return DateTime.ParseExact(input, "M/d/yyyy HH:mm:ss",
                        CultureInfo.InvariantCulture);
                }
                catch (Exception)
                {
                    try
                    {
                        return DateTime.ParseExact(input, "dd-M-yyyy HH:mm:ss",
                            CultureInfo.InvariantCulture);
                    }
                    catch (Exception)
                    {
                        try
                        {
                            return DateTime.ParseExact(input, "M-dd-yyyy HH:mm:ss",
                                CultureInfo.InvariantCulture);
                        }
                        catch (Exception)
                        {
                            return DateTime.ParseExact(input, "yyyy-MM-dd HH:mm:ss",
                           CultureInfo.InvariantCulture);
                        }
                    }
                }
            }
        }

        protected virtual ResponseModel<T> CallMicroSevice<T>(string url, string dataBody = "",
            HttpMethod httpMethod = HttpMethod.GET, string httpContentType = "application/json charset=utf-8")
        {
            try
            {
                var webClient = WebRequest.Create(url);
                webClient.Method = httpMethod.ToString();
                webClient.ContentType = httpContentType;
                if (dataBody.Trim() != string.Empty)
                {
                    using (var stream = webClient.GetRequestStream())
                    {
                        var dataByte = Encoding.UTF8.GetBytes(dataBody);
                        stream.Write(dataByte, 0, dataByte.Length);
                    }
                }

                using (var webResponse = webClient.GetResponse())
                {
                    var data = new StreamReader(webResponse.GetResponseStream());
                    string dataString = data.ReadToEnd();
                    var response = (T)JsonConvert.DeserializeObject(dataString, typeof(T));
                    return new ResponseModel<T>
                    {
                        ResponseCode = (int)HttpStatusCode.OK,
                        Data = response
                    };
                }
            }
            catch (WebException e)
            {
                return new ResponseModel<T>
                {
                    Message = e.Message,
                    ResponseCode = (int)HttpStatusCode.InternalServerError
                };
            }
        }

        protected virtual T MicroSeviceToMicroSevice<T>(string url, string dataBody = "",
            HttpMethod httpMethod = HttpMethod.GET, string httpContentType = "application/json")
        {
            try
            {
                var webClient = (HttpWebRequest)WebRequest.Create(url);
                webClient.Method = httpMethod.ToString();
                webClient.ContentType = httpContentType;
                webClient.Headers.Add("Token", AuthValue);
                webClient.ProtocolVersion = HttpVersion.Version10;
                webClient.Credentials = CredentialCache.DefaultCredentials;
                if (dataBody.Trim() != string.Empty)
                {
                    if (httpMethod == HttpMethod.GET)
                    {
                        throw new Exception("Phương thức 'Get' không thể chuyền body");
                    }

                    using (var stream = webClient.GetRequestStream())
                    {
                        var dataByte = Encoding.UTF8.GetBytes(dataBody);
                        stream.Write(dataByte, 0, dataByte.Length);
                    }
                }

                using (var webResponse = webClient.GetResponse())
                {
                    var data = new StreamReader(webResponse.GetResponseStream());
                    string dataString = data.ReadToEnd();
                    var response = (T)JsonConvert.DeserializeObject(dataString, typeof(T));
                    return response;
                }
            }
            catch (WebException ex)
            {
                if (ex.Response == null)
                {
                    throw ex;
                }

                using (var webResponse = ex.Response)
                {
                    var data = new StreamReader(webResponse.GetResponseStream());
                    string dataString = data.ReadToEnd();
                    var response = (T)JsonConvert.DeserializeObject(dataString, typeof(T));
                    return response;
                }
            }
        }

        protected virtual async Task<T> CallServiceViettelAsync<T>(string url, string dataBody = "",
            HttpMethod httpMethod = HttpMethod.GET, string httpContentType = "application/json")
        {
            return await Task<T>.Run(() =>
            {
                try
                {
                    var webClient = (HttpWebRequest)WebRequest.Create(url);
                    webClient.Method = httpMethod.ToString();
                    webClient.ContentType = httpContentType;
                    webClient.Headers.Add("Token", AuthValue);
                    webClient.ProtocolVersion = HttpVersion.Version10;
                    webClient.Credentials = CredentialCache.DefaultCredentials;
                    if (dataBody.Trim() != string.Empty)
                    {
                        if (httpMethod == HttpMethod.GET)
                        {
                            throw new Exception("Phương thức 'Get' không thể chuyền body");
                        }

                        using (var stream = webClient.GetRequestStream())
                        {
                            var dataByte = Encoding.UTF8.GetBytes(dataBody);
                            stream.Write(dataByte, 0, dataByte.Length);
                        }
                    }

                    using (var webResponse = webClient.GetResponse())
                    {
                        var data = new StreamReader(webResponse.GetResponseStream());
                        string dataString = data.ReadToEnd();
                        var response = (T)JsonConvert.DeserializeObject(dataString, typeof(T));
                        return response;
                    }
                }
                catch (WebException ex)
                {
                    if (ex.Response == null)
                    {
                        throw ex;
                    }

                    using (var webResponse = ex.Response)
                    {
                        var data = new StreamReader(webResponse.GetResponseStream());
                        string dataString = data.ReadToEnd();
                        var response = (T)JsonConvert.DeserializeObject(dataString, typeof(T));
                        return response;
                    }
                }
            });
        }

        protected virtual async Task<T> MicroSeviceToMicroSeviceAsync<T>(string url, string dataBody = "",
            HttpMethod httpMethod = HttpMethod.GET, string httpContentType = "application/json charset=utf-8")
        {
            return await Task<T>.Run(() =>
            {
                try
                {
                    var webClient = WebRequest.Create(url);
                    webClient.Method = httpMethod.ToString();
                    webClient.ContentType = httpContentType;
                    if (dataBody.Trim() != string.Empty)
                    {
                        if (httpMethod == HttpMethod.GET)
                        {
                            throw new Exception("Phương thức 'Get' không thể chuyền body");
                        }

                        using (var stream = webClient.GetRequestStream())
                        {
                            var dataByte = Encoding.UTF8.GetBytes(dataBody);
                            stream.Write(dataByte, 0, dataByte.Length);
                        }
                    }

                    using (var webResponse = webClient.GetResponse())
                    {
                        var data = new StreamReader(webResponse.GetResponseStream());
                        string dataString = data.ReadToEnd();
                        var response = (T)JsonConvert.DeserializeObject(dataString, typeof(T));
                        return response;
                    }
                }
                catch (WebException ex)
                {
                    if (ex.Response == null)
                    {
                        throw ex;
                    }

                    using (var webResponse = ex.Response)
                    {
                        var data = new StreamReader(webResponse.GetResponseStream());
                        string dataString = data.ReadToEnd();
                        var response = (T)JsonConvert.DeserializeObject(dataString, typeof(T));
                        return response;
                    }
                }
            });
        }

        protected virtual async Task<ResponseModel<T>> CallMicroSeviceAsync<T>(string url, string dataBody = "",
            HttpMethod httpMethod = HttpMethod.GET, string httpContentType = "application/json charset=utf-8")
        {
            return await Task<ResponseModel<T>>.Run(() =>
            {
                try
                {
                    var webClient = WebRequest.Create(url);
                    webClient.Method = httpMethod.ToString();
                    webClient.ContentType = httpContentType;
                    if (dataBody.Trim() != string.Empty)
                    {
                        using (var stream = webClient.GetRequestStream())
                        {
                            var dataByte = Encoding.UTF8.GetBytes(dataBody);
                            stream.Write(dataByte, 0, dataByte.Length);
                        }
                    }

                    using (var webResponse = webClient.GetResponse())
                    {
                        var data = new StreamReader(webResponse.GetResponseStream());
                        var response = (T)JsonConvert.DeserializeObject(data.ReadToEnd(), typeof(T));
                        return new ResponseModel<T>
                        {
                            ResponseCode = (int)HttpStatusCode.OK,
                            Data = response
                        };
                    }
                }
                catch (WebException e)
                {
                    return new ResponseModel<T>
                    {
                        Message = e.Message,
                        ResponseCode = (int)HttpStatusCode.InternalServerError
                    };
                }
            });
        }

        public bool ValidateAuthUser(out int employeeId, out string employeeEmail)
        {
            try
            {
                employeeId = int.Parse(HttpHelper.HttpContext?.Request?.Headers["User-Id"].ToString());
            }
            catch
            {
                throw new Exception("Nhân viên không hợp lệ");
            }

            employeeEmail = HttpHelper.HttpContext?.Request?.Headers["User-Username"].ToString();
            if (string.IsNullOrEmpty(employeeEmail)) throw new Exception("Nhân viên không hợp lệ");

            return true;
        }

        private static HttpClient _client;

        protected static HttpClient GetClient(string baseUri, string authToken = "")
        {
            var employeeId = 0;
            var employeeEmail = "";
            var employeeCode = "";
            try
            {
                employeeId = int.Parse(HttpHelper.HttpContext?.Request?.Headers["User-Id"].ToString());
            }
            catch
            {
                // throw new Exception("Nhân viên không hợp lệ");
            }

            employeeEmail = HttpHelper.HttpContext?.Request?.Headers["User-Username"].ToString();
            employeeCode = HttpHelper.HttpContext?.Request?.Headers["UserCode"].ToString();
            // if (string.IsNullOrEmpty(employeeEmail) || string.IsNullOrEmpty(employeeCode))
            // throw new Exception("Nhân viên không hợp lệ");

            if (_client == null)
            {
                _client = new HttpClient
                {
                    BaseAddress = new Uri(baseUri)
                };
                _client.Timeout = TimeSpan.FromSeconds(30);
            }

            _client.DefaultRequestHeaders.Remove("User-Username");
            _client.DefaultRequestHeaders.Add("User-Username", employeeEmail);
            _client.DefaultRequestHeaders.Remove("User-Id");
            _client.DefaultRequestHeaders.Add("User-Id", employeeId.ToString());
            _client.DefaultRequestHeaders.Remove("UserCode");
            //_client.DefaultRequestHeaders.Add("UserCode", employeeCode.ToString());

            if (!_client.DefaultRequestHeaders.Contains("Token"))
            {
                _client.DefaultRequestHeaders.Add("Token", authToken);
                //                _client.DefaultRequestHeaders.Remove("Token");
            }

            return _client;
        }

        public static async Task<string> PostApi(object model, string baseUri, string resource)
        {
            return await RequestApi(model, baseUri, resource, HttpMethod.POST);
        }

        public static async Task<string> GetApi(object model, string baseUri, string resource)
        {
            return await RequestApi(model, baseUri, resource, HttpMethod.GET);
        }

        public static async Task<T> PostApi<T>(object model, string baseUri, string resource)
        {
            return await RequestApi<T>(model, baseUri, resource, HttpMethod.POST);
        }

        public static async Task<T> GetApi<T>(object model, string baseUri, string resource)
        {
            return await RequestApi<T>(model, baseUri, resource, HttpMethod.GET);
        }

        public static async Task<T> RequestApi<T>(object model, string baseUri, string resource,
            HttpMethod method = HttpMethod.GET)
        {
            try
            {
                var responseString = string.Empty;
                var stringContent = JsonConvert.SerializeObject(model);
                if (stringContent.Length == 0 && (method == HttpMethod.PUT || method == HttpMethod.POST))
                {
                    throw new Exception("Dữ liệu không hợp lệ!");
                }

                var content = new StringContent(stringContent, Encoding.UTF8, "application/json");

                HttpResponseMessage response = new HttpResponseMessage();
                switch (method)
                {
                    case HttpMethod.GET:
                        response = await GetClient(baseUri).GetAsync(resource);
                        break;
                    case HttpMethod.POST:
                        //return  await CallServiceViettelAsync<T>(BaseUri + resource,stringContent,HttpMethod.POST);
                        response = await GetClient(baseUri).PostAsync(resource, content);
                        break;
                }

                responseString = await response.Content.ReadAsStringAsync();
                if (!response.IsSuccessStatusCode)
                {
                    // Log exception here
                    //                    LogError(message: "Kết nối API thất bại: " + response.StatusCode, new Exception(responseString));
                    throw new Exception("Kết nối API thất bại!");
                }
                else
                {
                    //                    LogInfo($"Request API sucessfull. Data received: {responseString}");
                }

                if (typeof(T) == typeof(string) || typeof(T) == typeof(bool) || typeof(T) == typeof(int))
                {
                    return (T)Convert.ChangeType(responseString, typeof(T), CultureInfo.InvariantCulture);
                }

                var result = JsonConvert.DeserializeObject<T>(responseString);
                return result;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public static async Task<T> PostApiWithHeaders<T>(object model, string baseUri, string resource, List<RequestHeaders> headers)
        {
            return await RequestApiWithHeaders<T>(model, baseUri, resource, HttpMethod.POST, headers);
        }
        public static async Task<T> GetApiWithHeaders<T>(object model, string baseUri, string resource, List<RequestHeaders> headers)
        {
            return await RequestApiWithHeaders<T>(model, baseUri, resource, HttpMethod.GET, headers);
        }

        public static async Task<T> RequestApiWithHeaders<T>(object model, string baseUri, string resource,
            HttpMethod method = HttpMethod.GET, List<RequestHeaders> headers = null)
        {
            try
            {
                var responseString = string.Empty;
                var stringContent = JsonConvert.SerializeObject(model);
                if (stringContent.Length == 0 && (method == HttpMethod.PUT || method == HttpMethod.POST))
                {
                    throw new Exception("Dữ liệu không hợp lệ!");
                }

                var content = new StringContent(stringContent, Encoding.UTF8, "application/json");
                var client = GetClient(baseUri);
                // Set additional headers
                if (headers != null)
                {
                    client.DefaultRequestHeaders.Clear();
                    foreach (var header in headers)
                    {
                        client.DefaultRequestHeaders.Add(header.Name, header.Value.ToString());
                    }
                }
                HttpResponseMessage response = new HttpResponseMessage();
                switch (method)
                {
                    case HttpMethod.GET:
                        response = await client.GetAsync(resource);
                        break;
                    case HttpMethod.POST: 
                        response = await client.PostAsync(resource, content);
                        break;
                }

                responseString = await response.Content.ReadAsStringAsync();
                if (!response.IsSuccessStatusCode)
                {
                    // Log exception here
                    //                    LogError(message: "Kết nối API thất bại: " + response.StatusCode, new Exception(responseString));
                    throw new Exception("Kết nối API thất bại!");
                }
                else
                {
                    //                    LogInfo($"Request API sucessfull. Data received: {responseString}");
                }

                if (typeof(T) == typeof(string) || typeof(T) == typeof(bool) || typeof(T) == typeof(int))
                {
                    return (T)Convert.ChangeType(responseString, typeof(T), CultureInfo.InvariantCulture);
                }

                var result = JsonConvert.DeserializeObject<T>(responseString);
                return result;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public static async Task<string> RequestApi(object model, string baseUri, string resource,
            HttpMethod method = HttpMethod.GET)
        {
            try
            {
                var stringContent = JsonConvert.SerializeObject(model);
                if (stringContent.Length == 0 && (method == HttpMethod.PUT || method == HttpMethod.POST))
                {
                    throw new Exception("Dữ liệu không hợp lệ!");
                }

                var content = new StringContent(stringContent, Encoding.UTF8, "application/json");

                HttpResponseMessage response = new HttpResponseMessage();
                switch (method)
                {
                    case HttpMethod.GET:
                        response = await GetClient(baseUri).GetAsync(resource);
                        break;
                    case HttpMethod.POST:
                        response = await GetClient(baseUri).PostAsync(resource, content);
                        break;
                }

                var responseString = await response.Content.ReadAsStringAsync();
                if (!response.IsSuccessStatusCode)
                {
                    // Log exception here
                    throw new Exception("Kết nối API thất bại!");
                }

                return responseString;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public void LogInfo(String message = "", params object[] args)
        {
            _logger.LogInformation(message, args);
        }

        public void LogError(String message = "", Exception ex = null, params object[] args)
        {
            _logger.LogError(ex, message, args);
        }

        public string ConvertDateTime(string input)
        {
            string output = input;
            DateTime inputDate = DateTime.MinValue;

            string predefinedFormat = "dd/M/yyyy HH:mm:ss";

            if (DateTime.TryParseExact(input, predefinedFormat, CultureInfo.InvariantCulture,
                DateTimeStyles.None, out inputDate))
            {
                // Input is already formatted, return itself
                return input;
            }

            if (input.Contains("Z"))
            {
                TimeZoneInfo infotime;
                try
                {
                    infotime = TimeZoneInfo.FindSystemTimeZoneById("SE Asia Standard Time");
                }
                catch (Exception)
                {
                    // Not Windows system, try to find under Unix-based system
                    infotime = TimeZoneInfo.FindSystemTimeZoneById("Asia/Ho_Chi_Minh");
                }

                inputDate = TimeZoneInfo.ConvertTimeFromUtc(
                    DateTime.Parse(input.Replace("T", " ")
                        .Replace("Z", " ")), infotime);

                output = inputDate.ToString("dd/M/yyyy HH:mm:ss");
            }

            return output;
        }
    }
}
