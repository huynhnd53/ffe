﻿using Azure.Core;

using FFE.Entities;
using FFE.Models;
using FFE.Models.ConfigAttendDailys;

using Microsoft.EntityFrameworkCore;

using static FFE.Common.EnumBase;
using static FFE.Models.AttendedDaily.AttendedDailyResponseModel;

namespace FFE.Services
{
    public interface IAttendedDailyService
    {
        Task<ResponseModel<List<UserAttendDailyReportResponseModel>>> UsersAttendedMonth(int month, int year, string username);
        Task<ResponseModel<bool>> UserAttendedDaily(int day, int month, int year, string username);
    }


    public class AttendedDailyService : IAttendedDailyService
    {
        FFEContext _dbContext;
        public AttendedDailyService(FFEContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<ResponseModel<List<UserAttendDailyReportResponseModel>>> UsersAttendedMonth(int month, int year, string username)
        {
            ResponseModel<List<UserAttendDailyReportResponseModel>> response = new();
            List<UserAttendDailyReportResponseModel> lstData = new();
            try
            {
                var configAttendDaily = _dbContext.TblConfigAttendDailys.Where(x => x.ConfigMonth == month && x.ConfigYear == year).ToList();

                if (configAttendDaily == null || configAttendDaily.Count == 0)
                {
                    response.Message = "Không tồn tại cấu hình Điểm danh cho Tháng " + month + " năm " + year;
                    return response;
                }

                var userAttendDaily = _dbContext.TblAttendedDailys.Where(x => x.ConfigMonth == month && x.ConfigYear == year && x.Username == username)
                                    .ToDictionary(x => x.ConfigDay ?? 0, x => x);

                foreach (var item in configAttendDaily)
                {
                    lstData.Add(new UserAttendDailyReportResponseModel
                    {
                        AttendDate = item.AttendDate,
                        ConfigDay = item.ConfigDay,
                        ConfigMonth = item.ConfigMonth,
                        ConfigYear = item.ConfigYear,
                        Points = item.Points ?? 0,
                        IsAttended = userAttendDaily.ContainsKey(item.ConfigDay),
                    });
                }
                response.SetSuccess();
                response.Data = lstData;
            }
            catch (Exception ex)
            {
            }
            return response;
        }

        public async Task<ResponseModel<bool>> UserAttendedDaily(int day, int month, int year, string username)
        {
            ResponseModel<bool> response = new();
            try
            {
                DateTime dateCrr = DateTime.Now;
                if (day == 0 || month == 0 || year == 0)
                    return response;

                var user = _dbContext.TblUsers.Where(x => x.Username == username).FirstOrDefault();
                if (user == null)
                    return response;

                var tokenInfo = _dbContext.TblTokens.Where(x => x.Id == (int)EnumToken.WREGE).FirstOrDefault();
                var wregWallet = _dbContext.TblWalletDetails.Where(x => x.TokenId == (int)EnumToken.WREG && x.UserId == user.Id).FirstOrDefault();

                var config = _dbContext.TblConfigAttendDailys.FirstOrDefault(x => x.ConfigDay == day && x.ConfigMonth == month && x.ConfigYear == year);
                if (config == null)
                {
                    response.Message = "Chưa có cấu hình Điểm danh";
                    return response;
                }
                if (config.AttendDate.Day < dateCrr.Day)
                {
                    response.Message = "Quá hạn Điểm danh";
                    return response;
                }
                if (_dbContext.TblAttendedDailys.Any(x => x.ConfigDay == day && x.ConfigMonth == month && x.ConfigYear == year && x.Username == username))
                {
                    response.Message = "Bạn đã Điểm danh ngày này";
                    return response;
                }

                _dbContext.TblAttendedDailys.Add(new TblAttendedDailys
                {
                    ConfigAttendDailyId = config.Id,
                    AttendDate = config.AttendDate,
                    ConfigYear = config.ConfigYear,
                    ConfigMonth = config.ConfigMonth,
                    ConfigDay = config.ConfigDay,
                    UserId = user.Id,
                    Username = user.Username,
                });

                if (wregWallet == null)
                {
                    _dbContext.TblWalletDetails.Add(new TblWalletDetails
                    {
                        UserId = user.Id,
                        Username = user.Username,
                        Price = tokenInfo.Price,
                        TokenId = tokenInfo.Id,
                        TokenName = tokenInfo.TokenName,
                        TokenCode = tokenInfo.TokenCode,
                        TokenAddressBep20 = tokenInfo.ContractCodeBep20,
                        TokenAddressERC20 = tokenInfo.ContractCodeERC20,
                        BalanceAvaiable = config.Points,
                        Balance = config.Points,
                    });
                }
                else
                {
                    wregWallet.Balance += config.Points;
                    wregWallet.BalanceAvaiable += config.Points;
                    _dbContext.TblWalletDetails.Update(wregWallet);
                }

                _dbContext.TblTransactions.Add(new TblTransactions
                {
                    TransactionSource = 0,
                    TransactionSourceUsername = "system",
                    TransactionTarget = user.Id,
                    TransactionTargetUsername = user.Username,
                    TransactionType = (int)EnumTransactionType.AttendedDaily,
                    Descriptions = "Nhận " + config.Points + " " + EnumToken.WREG.ToString() + " từ Điểm danh hàng ngày",
                    Amount = config.Points,
                    TokenCode = EnumToken.WREG.ToString(),
                    TokenId = (long)EnumToken.WREG,
                    Status = (int)EnumTransactionStatus.Success
                });
                await _dbContext.SaveChangesAsync();
                response.SetSuccess();
                response.Data = true;
            }
            catch (Exception ex)
            {
            }
            return response;
        }
    }
}
