﻿using FFE.Entities;
using FFE.Models;
using FFE.Models.AuthenticationModels;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using FFE.Services.Interfaces;
using Azure;
using FFE.Models.ServicesAPIModels;
using Azure.Core;
using Microsoft.EntityFrameworkCore;
using System.Net;
using FFE.Models.Wallet;

namespace FFE.Services
{
    public class AuthenticationService : IAuthenticationService
    {
        private readonly FFEContext _dbContext;
        private readonly JWTSettings _jwtsettings;
        private readonly IServiceAPI _serviceAPI;
        private readonly IWalletService _walletService;

        public AuthenticationService(FFEContext context, IOptions<JWTSettings> jwtsettings
            , IServiceAPI serviceAPI, IWalletService walletService)
        {
            _dbContext = context;
            _jwtsettings = jwtsettings.Value;
            _serviceAPI = serviceAPI;
            _walletService = walletService;
        }
        public async Task<ResponseModel<UsersResponseModel>> Login(LoginRequestModel request)
        {
            var output = new ResponseModel<UsersResponseModel>();
            var usersView = new UsersResponseModel();
            string? userId = "0";
            string? tokenAppRaw = "0";
            string? refreshTokenAppRaw = "0";
            output.Data = null;
            var isUser = false;
            try
            {
                if (!string.IsNullOrEmpty(request.Username) && !string.IsNullOrEmpty(request.Password))
                {
                    string pass = request.Password;
                    #region check user
                    var usertExist = _dbContext.TblUsers.Where(x => !string.IsNullOrEmpty(x.Username) && x.Username.Trim().ToLower().Equals(request.Username.Trim().ToLower())).FirstOrDefault();
                    //nếu user có trong hệ thống và mật khẩu đúng => true => đăng nhập thành công
                    if (usertExist != null && !usertExist.IsUserAppRaw && IsBcryptPassword(pass, usertExist.Password))
                    {
                        usersView = MapUserToUsersResponse(usertExist);
                        userId = usertExist.Id.ToString();
                        isUser = true;
                    }
                    //nếu user có trong hệ thống và user nguồn từ appRaw  => call sang api lấy thông tin user app ffe
                    else if (usertExist != null && usertExist.IsUserAppRaw)
                    {
                        var userLoginAppRaw = await _serviceAPI.LoginViaFfeAppRaw(new LoginappffeRequestModel { email = request.Username, password = request.Password });
                        //lấy thông tin từ api trả về
                        if (userLoginAppRaw != null)
                        {
                            if (!userLoginAppRaw.status)
                            {
                                output.Message = userLoginAppRaw.message;
                                return output;
                            }
                            usersView = MapFFEAppUsersToUsersResponse(userLoginAppRaw.user);
                            tokenAppRaw = userLoginAppRaw.accessToken ?? "";
                            refreshTokenAppRaw = userLoginAppRaw.refreshToken ?? "";
                            userId = userLoginAppRaw.user._id;
                            isUser = true;
                        }
                    }
                    //nếu user ko có trong hệ thống => call sang api lấy thông tin user appRaw
                    else
                    {
                        var userLoginAppRaw = await _serviceAPI.LoginViaFfeAppRaw(new LoginappffeRequestModel { email = request.Username, password = request.Password });
                        //nếu có user tồn tại bên appRaw => tạo bản ghi user ở hệ thống hiện tại 
                        if (userLoginAppRaw != null)
                        {
                            if (!userLoginAppRaw.status)
                            {
                                output.Message = userLoginAppRaw.message;
                                return output;
                            }
                            usersView = MapFFEAppUsersToUsersResponse(userLoginAppRaw.user);
                            tokenAppRaw = userLoginAppRaw.accessToken ?? "";
                            refreshTokenAppRaw = userLoginAppRaw.refreshToken ?? "";
                            userId = userLoginAppRaw.user._id;
                            isUser = true;
                            #region bổ sung code phần lưu user của appraw vào hệ thống hiện tại
                            await _dbContext.TblUsers.AddAsync(new TblUsers
                            {
                                Username = userLoginAppRaw.user.email,
                                IdAppRaw = userLoginAppRaw.user._id,
                                PasswordAppRaw = userLoginAppRaw.user.password,
                                Fullname = userLoginAppRaw.user.fullName,
                                Email = userLoginAppRaw.user.email,
                                IsVerified = userLoginAppRaw.user.isVerified,
                                AffiliateLevel = userLoginAppRaw.user.affiliateLevel,
                                MyRef = userLoginAppRaw.user.myRef,
                                UserType = userLoginAppRaw.user.type,
                                IsUserAppRaw = true,
                            });
                            await _dbContext.SaveChangesAsync();
                            #endregion
                        }
                    }
                    #endregion

                    if (isUser)
                    {
                        usersView.TotalMember = _dbContext.TblUsers.Count();
                        var wallet = await _walletService.MyWalletDetail(usersView.Username);
                        if (wallet != null && wallet.ResponseCode == (int)HttpStatusCode.OK)
                            usersView.Wallet = wallet.Data;
                        else
                            usersView.Wallet = new WalletResponseModel();

                        #region create token 
                        var tokenHandler = new JwtSecurityTokenHandler();
                        var key = Encoding.UTF8.GetBytes(_jwtsettings.SecretKey);
                        Array.Resize(ref key, 16);
                        var tokenDescription = new SecurityTokenDescriptor
                        {
                            //add user session
                            Subject = new ClaimsIdentity(new Claim[] {
                                    new Claim("Username", usersView.Username),
                                    new Claim("Fullname", usersView.Fullname??""),
                                    new Claim("UserId", userId),
                                    new Claim("TokenAppRaw", tokenAppRaw),
                                    new Claim("RefreshTokenAppRaw", refreshTokenAppRaw),
                                    new Claim("TotalMember", usersView.TotalMember.ToString()),
                                }),
                            Expires = DateTime.Now.AddDays(1),
                            SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key),
                            SecurityAlgorithms.HmacSha256Signature)
                        };
                        var token = tokenHandler.CreateToken(tokenDescription);
                        usersView.Token = tokenHandler.WriteToken(token);
                        usersView.TokenAppRaw = tokenAppRaw;
                        usersView.RefreshTokenAppRaw = refreshTokenAppRaw;
                        #endregion
                        output.Data = usersView;
                        output.SetSuccess();
                        return output;
                    }

                    output.Message = "username or password was wrong";
                    return output;
                }
                output.Message = "Username or password not allow empty";
                return output;
            }
            catch (Exception ex)
            {
            }
            return output;
        }
        public async Task<ResponseModel<bool>> IsExistedRefCode(RefCodeRequestModel request)
        {
            var output = new ResponseModel<bool>();
            try
            {
                if (string.IsNullOrEmpty(request.refCode))
                {
                    output.Message = "Vui lòng nhập RefCode";
                    return output;
                }

                if (_dbContext.TblUsersAppRaw.Any(x => x.myRef == request.refCode))
                {
                    output.Data = true;
                    output.SetSuccess();
                }
                else
                    output.Message = "RefCode không tồn tại";
            }
            catch (Exception)
            {
            }
            return output;
        }
        public async Task<ResponseModel<bool>> SignUp(SignUpRequestModel request)
        {
            var output = new ResponseModel<bool>();
            try
            {
                var userSignUpAppRaw = await _serviceAPI.SignUp(new SignUpRequestModel
                {
                    fullname = request.fullname,
                    email = request.email,
                    password = request.password,
                    repeatPassword = request.repeatPassword,
                    refCode = request.refCode
                });
                if (userSignUpAppRaw != null)
                {
                    if (userSignUpAppRaw.status)
                        output.SetSuccess();

                    output.Data = userSignUpAppRaw.status;
                    output.Message = userSignUpAppRaw.message;
                    return output;
                }
            }
            catch (Exception ex)
            {
            }
            return output;
        }
        public async Task<ResponseModel<bool>> ConfirmOTPMailSignUp(ConfirmOTPRequestModel request)
        {
            var output = new ResponseModel<bool>();
            try
            {
                var userConfirmTOPAppRaw = await _serviceAPI.ConfirmOTPMailSignUp(new ConfirmOTPRequestModel
                {
                    email = request.email,
                    code = request.code
                });
                if (userConfirmTOPAppRaw != null)
                {
                    if (userConfirmTOPAppRaw.status)
                        output.SetSuccess();

                    output.Data = userConfirmTOPAppRaw.status;
                    output.Message = userConfirmTOPAppRaw.message;
                    return output;
                }
            }
            catch (Exception ex)
            {
            }
            return output;
        }
        public async Task<ResponseModel<bool>> ForgetPassword(VerifyEmailRequestModel request)
        {
            var output = new ResponseModel<bool>();
            try
            {
                var userRequestConfirmMailAppRaw = await _serviceAPI.RequestConfirmEmail(new VerifyEmailRequestModel
                {
                    email = request.email,
                });
                if (userRequestConfirmMailAppRaw != null)
                {
                    if (userRequestConfirmMailAppRaw.status)
                        output.SetSuccess();

                    output.Data = userRequestConfirmMailAppRaw.status;
                    output.Message = userRequestConfirmMailAppRaw.message;
                }
            }
            catch (Exception ex)
            {
            }
            return output;
        }
        public async Task<ResponseModel<bool>> VerifyEmail(VerifyEmailRequestModel request)
        {
            var output = new ResponseModel<bool>();
            try
            {
                var userRequestConfirmMailAppRaw = await _serviceAPI.RequestConfirmEmail(new VerifyEmailRequestModel
                {
                    email = request.email,
                });
                if (userRequestConfirmMailAppRaw != null)
                {
                    if (userRequestConfirmMailAppRaw.status)
                        output.SetSuccess();

                    output.Data = userRequestConfirmMailAppRaw.status;
                    output.Message = userRequestConfirmMailAppRaw.message;
                }
            }
            catch (Exception ex)
            {
            }
            return output;
        }
        public async Task<ResponseModel<bool>> SetNewPassword(SetNewPasswordRequestModel request)
        {
            var output = new ResponseModel<bool>();
            try
            {
                var userSetNewPassword = await _serviceAPI.RequestSetNewPassword
                    (new SetNewPasswordModel { newPassword = request.newPassword, repeatNewPassword = request.repeatNewPassword }, request.accessToken);

                if (userSetNewPassword != null)
                {
                    if (userSetNewPassword.status)
                        output.SetSuccess();

                    output.Data = userSetNewPassword.status;
                    output.Message = userSetNewPassword.message;
                }
            }
            catch (Exception ex)
            {
            }
            return output;
        }
        public async Task<appffeConfirmOTPResponseModel> ConfirmOTPForgotPassword(ConfirmOTPRequestModel request)
        {
            return await _serviceAPI.ConfirmOTPForgotPassRequest(new ConfirmOTPRequestModel
            {
                email = request.email,
                code = request.code
            });
        }
        public async Task<ResponseModel<bool>> SyncUserAppRaw(string? passcode)
        {
            var output = new ResponseModel<bool>();

            if (passcode != "huynhcute")
                return new ResponseModel<bool>() { Message = "Sai passcode" };

            List<TblUsersAppRaw> lstUserRaw = new();
            try
            {
                var response = await _serviceAPI.GetListUsers();
                if (response != null)
                {
                    if (response.users.Any())
                    {
                        lstUserRaw.AddRange(response.users.Select(x => MapUserRawToUser(x)).ToList());
                        await _dbContext.TblUsersAppRaw.AddRangeAsync(lstUserRaw);
                        await _dbContext.SaveChangesAsync();
                    }
                    output.Data = response.status;
                    output.SetSuccess();
                }
            }
            catch (Exception ex)
            {
            }
            return output;
        }
        public async Task<ResponseModel<UsersResponseModel>> UserInfo(string username, string tokenaccess)
        {

            ResponseModel<UsersResponseModel> response = new();
            var usersView = new UsersResponseModel();
            try
            {
                #region check user
                var usertExist = _dbContext.TblUsers.Where(x => x.Username.Trim().ToLower().Equals(username.Trim().ToLower())).FirstOrDefault();
                if (usertExist != null)
                {
                    if(!usertExist.IsUserAppRaw)
                        usersView = MapUserToUsersResponse(usertExist);
                    else if (usertExist.IsUserAppRaw)
                    {
                        var userLoginAppRaw = await _serviceAPI.UsersProfile(tokenaccess);
                        //lấy thông tin từ api trả về
                        if (userLoginAppRaw != null)
                            usersView = MapFFEAppUsersToUsersResponse(userLoginAppRaw.data);

                        response.Message = userLoginAppRaw.message;
                    }

                    usersView.TotalMember = _dbContext.TblUsers.Count();

                    var wallet = await _walletService.MyWalletDetail(usertExist.Username);
                    if (wallet != null && wallet.ResponseCode == (int)HttpStatusCode.OK)
                        usersView.Wallet = wallet.Data;
                    else
                        usersView.Wallet = new WalletResponseModel();

                    response.Data = usersView;
                    response.SetSuccess();
                }
                #endregion
            }
            catch (Exception ex)
            {
            }
            return response;
        }
        public async Task SaveConnectChat(string connectionId)
        {
            var x = connectionId;
            await Console.Out.WriteLineAsync(x);
        }

        private bool IsBcryptPassword(string? passwordInput, string? passwordRaw)
        {
            return BCrypt.Net.BCrypt.Verify(passwordInput, passwordRaw);
        }
        private TblUsersAppRaw MapUserRawToUser(FFEAppUsersModel userRaw)
        {

            return new TblUsersAppRaw
            {
                _id = userRaw._id,
                email = userRaw.email,
                password = userRaw.password,
                isVerified = userRaw.isVerified,
                fullName = userRaw.fullName,
                affiliateLevel = userRaw.affiliateLevel,
                type = userRaw.type,
                isChangePassWord = userRaw.isChangePassWord,
                isLock = userRaw.isLock,
                isDelete = userRaw.isDelete,
                myRef = userRaw.myRef,
                totalOrder = 0,
                totalRevenue = 0,
                totalMoney = 0,
                totalPurchase = 0,
                totalCommission = 0,
                lastLogin = userRaw.lastLogin,
                createdAt = userRaw.createdAt,
                updatedAt = userRaw.updatedAt,
                birthDay = userRaw.birthDay,
                phoneNumber = userRaw.phoneNumber,
                __v = userRaw.__v,
                parentID = userRaw.parentID,
            };
        }
        private UsersResponseModel MapFFEAppUsersToUsersResponse(FFEAppUsersLoginResponseModel ffeAppUsers)
        {
            return new UsersResponseModel
            {
                Username = ffeAppUsers.email,
                IdAppRaw = ffeAppUsers._id,
                Email = ffeAppUsers.email,
                Fullname = ffeAppUsers.fullName,
                TotalOrders = (int)ffeAppUsers.totalOrder,
                TotalRevenue = ffeAppUsers.totalRevenue,
                TotalMoney = ffeAppUsers.totalMoney,
                TotalPurchased = ffeAppUsers.totalPurchase,
                TotalCommission = ffeAppUsers.totalCommission,
                Phone = ffeAppUsers.phoneNumber,
                DateOfBirth = ffeAppUsers.birthDay,
                IsVerified = ffeAppUsers.isVerified,
                AffiliateLevel = ffeAppUsers.affiliateLevel,
                MyRef = ffeAppUsers.myRef,
                //UserType = ffeAppUsers.type,
                IsUserAppRaw = true
            };
        }
        private UsersResponseModel MapUserToUsersResponse(TblUsers tblUsers)
        {
            return new UsersResponseModel
            {
                Username = tblUsers.Username,
                IdAppRaw = tblUsers.IdAppRaw,
                Email = tblUsers.Email,
                Fullname = tblUsers.Fullname,
                TotalOrders = tblUsers.TotalOrders,
                TotalRevenue = tblUsers.TotalRevenue,
                TotalMoney = tblUsers.TotalMoney,
                TotalPurchased = tblUsers.TotalPurchased,
                TotalCommission = tblUsers.TotalCommission,
                Phone = tblUsers.Phone,
                DateOfBirth = tblUsers.DateOfBirth,
                IsVerified = tblUsers.IsVerified,
                AffiliateLevel = tblUsers.AffiliateLevel,
                MyRef = tblUsers.MyRef,
                UserType = tblUsers.UserType,
                IsUserAppRaw = tblUsers.IsUserAppRaw,
                Gender = tblUsers.Gender,
                Status = tblUsers.Status,
            };
        }

       
    }
}
