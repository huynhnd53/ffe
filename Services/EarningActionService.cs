﻿using Azure.Core;

using FFE.Entities;
using FFE.Models;
using FFE.Models.Earn;

using Microsoft.EntityFrameworkCore;

using System.Net;
using System.Reflection.Metadata;
using System.Threading.Tasks;

using static FFE.Common.EnumBase;

namespace FFE.Services
{
    public interface IEarningActionService
    {
        Task<ResponseModel<bool>> ChangeEarningAction(EnumEarningAction action, string username);
        Task<ResponseModel<bool>> StartEarning(string username);
        Task<ResponseModel<HasStartEarnResponseModel>> HasStartEarn(string username);
    }


    public class EarningActionService : IEarningActionService
    {
        FFEContext _dbContext;
        public EarningActionService(FFEContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<ResponseModel<bool>> ChangeEarningAction(EnumEarningAction action, string username)
        {
            DateTime dateCrr = DateTime.Now;
            ResponseModel<bool> response = new();
            try
            {
                var tokenWREGE = _dbContext.TblTokens.Where(x => x.Id == (int)EnumToken.WREGE).FirstOrDefault();
                var userExist = _dbContext.TblUsers.Where(x => x.Username == username).FirstOrDefault();

                var isExistAction = _dbContext.TblEarningActions.Where(x => x.Username == username).FirstOrDefault();
                if (isExistAction == null)
                {
                    if (action == EnumEarningAction.Stop)
                    {
                        response.Message = "Không tồn tại Hoạt động đào nào. Vui lòng kiểm tra lại";
                        return response;
                    }
                    else if (action == EnumEarningAction.Start)
                    {
                        _dbContext.TblEarningActions.Add(new TblEarningActions
                        {
                            ActionType = (int)EnumEarningAction.Start,
                            Username = username,
                            CreatedBy = username,
                            UpdatedBy = username,
                            CreatedDate = dateCrr,
                            UpdatedDate = dateCrr
                        });
                    }
                }
                else
                {
                    if (isExistAction.ActionType == (int)action)
                    {
                        response.Message = "Hoạt động đã ở trạng thái này.";
                        return response;
                    }
                    else if (action == EnumEarningAction.Stop)
                    {
                        var walletDetail = _dbContext.TblWalletDetails.Where(x => x.Username == username && x.TokenId == tokenWREGE.Id).FirstOrDefault();
                        TimeSpan difference = dateCrr - isExistAction.UpdatedDate;
                        decimal totalReceive = (decimal)difference.TotalMinutes * FFE.Common.Constant.EARN_PER_MIN;
                        if (walletDetail != null)
                        {
                            walletDetail.BalanceAvaiable += totalReceive;
                            walletDetail.Balance += totalReceive;
                            walletDetail.UpdatedDate = dateCrr;
                            walletDetail.UpdatedBy = userExist.Username;
                            _dbContext.TblWalletDetails.Update(walletDetail);
                        }
                        else
                        {
                            _dbContext.TblWalletDetails.Add(new TblWalletDetails
                            {
                                UserId = userExist.Id,
                                Username = username,
                                Price = tokenWREGE.Price,
                                TokenId = tokenWREGE.Id,
                                TokenName = tokenWREGE.TokenName,
                                TokenCode = tokenWREGE.TokenCode,
                                TokenAddressBep20 = tokenWREGE.ContractCodeBep20,
                                TokenAddressERC20 = tokenWREGE.ContractCodeERC20,
                                BalanceAvaiable = totalReceive,
                                Balance = totalReceive,
                            });
                        }

                        _dbContext.TblTransactions.Add(new TblTransactions
                        {
                            TransactionSource = 0,
                            TransactionSourceUsername = "System",
                            TransactionTarget = userExist.Id,
                            TransactionTargetUsername = userExist.Username,
                            TransactionType = (int)EnumTransactionType.SendToken,
                            Descriptions = "Nhận " + totalReceive + " " + tokenWREGE.TokenCode + " từ Earning",
                            Amount = totalReceive,
                            TokenCode = tokenWREGE.TokenCode,
                            TokenId = tokenWREGE.Id,
                            Status = (int)EnumTransactionStatus.Success
                        });

                        isExistAction.ActionType = (int)EnumEarningAction.Stop;
                        isExistAction.UpdatedDate = dateCrr;
                        isExistAction.UpdatedBy = userExist.Username;
                        _dbContext.TblEarningActions.Update(isExistAction);
                    }
                    else if (action == EnumEarningAction.Start)
                    {
                        _dbContext.TblEarningActions.Add(new TblEarningActions
                        {
                            ActionType = (int)EnumEarningAction.Start,
                            Username = username,
                            UpdatedDate = dateCrr,
                            UpdatedBy = username
                        });
                    }
                }
                await _dbContext.SaveChangesAsync();
                response.Data = true;
                response.SetSuccess();
            }
            catch (Exception ex)
            {
            }
            return response;
        }

        public async Task<ResponseModel<bool>> StartEarning(string username)
        {
            DateTime dateCrr = DateTime.Now;
            ResponseModel<bool> response = new();
            try
            {
                if (!await IsEarningExist(username))
                {
                    var tokenWREGE = _dbContext.TblTokens.Where(x => x.Id == (int)EnumToken.WREG).FirstOrDefault();
                    var userExist = _dbContext.TblUsers.Where(x => x.Username.Equals(username)).FirstOrDefault();
                    var userWalletCrr = _dbContext.TblWalletDetails.Where(x => x.UserId == userExist.Id && x.TokenId.Value == (long)EnumToken.WREG).FirstOrDefault();
                    decimal totalReceive = 86400 * FFE.Common.Constant.EARN_PER_MIN;

                    if (userWalletCrr != null)
                    {
                        userWalletCrr.BalanceAvaiable += totalReceive;
                        userWalletCrr.Balance += totalReceive;
                        userWalletCrr.UpdatedDate = dateCrr;
                        userWalletCrr.UpdatedBy = userExist.Username;
                        _dbContext.TblWalletDetails.Update(userWalletCrr);
                    }
                    else
                    {
                        _dbContext.TblWalletDetails.Add(new TblWalletDetails
                        {
                            UserId = userExist.Id,
                            Username = username,
                            Price = tokenWREGE.Price,
                            TokenId = tokenWREGE.Id,
                            TokenName = tokenWREGE.TokenName,
                            TokenCode = tokenWREGE.TokenCode,
                            TokenAddressBep20 = tokenWREGE.ContractCodeBep20,
                            TokenAddressERC20 = tokenWREGE.ContractCodeERC20,
                            BalanceAvaiable = totalReceive,
                            Balance = totalReceive,
                        });
                    }

                    _dbContext.Add(new TblEarningActions
                    {
                        ActionType = (int)EnumEarningAction.Mined,
                        Username = username,
                    });

                    await _dbContext.SaveChangesAsync();
                    response.SetSuccess();
                    response.Data = true;
                }
                else
                    response.Message = "Bạn đang trong phiên đào, hãy chờ kết thúc phiên này";
            }
            catch (Exception ex)
            {
            }
            return response;
        }

        public async Task<ResponseModel<HasStartEarnResponseModel>> HasStartEarn(string username)
        {
            ResponseModel<HasStartEarnResponseModel> response = new();
            try
            {
                var isEarningExist = await IsEarningExist(username);
                response.SetSuccess();
                if (isEarningExist)
                {
                    response.Message = "Bạn đang trong phiên đào.";
                    response.Data = new HasStartEarnResponseModel
                    {
                        StartedTime = _dbContext.TblEarningActions.AsEnumerable().Where(x => x.Username.Equals(username)).OrderByDescending(x => x.CreatedDate).FirstOrDefault().CreatedDate
                    };
                }
                else
                    response.Message = "Bạn không trong phiên đào nào.";
            }
            catch (Exception ex)
            {
            }
            return response;
        }


        private async Task<bool> IsEarningExist(string username)
        {
            DateTime dcr = DateTime.Now;
            var lstaction = _dbContext.TblEarningActions.AsEnumerable().Where(x => x.Username.Equals(username) && dcr - x.CreatedDate <= TimeSpan.FromSeconds(86400)).ToList();
            if (lstaction.Any())
                return true;
            return false;
        }

        private async Task<bool> IsAttendedExist(string username)
        {
            DateTime startOfDay = DateTime.Today;
            DateTime endOfDay = DateTime.Today.AddDays(1).AddTicks(-1);
            return true;
        }
    }
}
