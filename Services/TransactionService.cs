﻿using FFE.Models.Posts;
using FFE.Models;
using FFE.Models.Transaction;
using FFE.Entities;
using static FFE.Common.EnumBase;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Azure.Core;

namespace FFE.Services
{
    public interface ITransactionService
    {
        Task<PagingResponseModel<TransactionResponseModel>> GetPagingTransaction(TransactionRequestModel request, string username);
        Task<PagingResponseModel<SearchUsersTransactionResponseModel>> SearchUsersReceiveToken(SearchUsersTransactionRequestModel request, string username);
        Task<ResponseModel<bool>> SendToken(SendTokenTransactionRequestModel request, string username);
        Task<ResponseModel<bool>> ClaimEarnToken(decimal amountClaim = 0, string username = "");
    }
    public class TransactionService : ITransactionService
    {
        FFEContext _dbContext;
        public TransactionService(FFEContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<PagingResponseModel<SearchUsersTransactionResponseModel>> SearchUsersReceiveToken(SearchUsersTransactionRequestModel request, string username)
        {
            PagingResponseModel<SearchUsersTransactionResponseModel> response = new()
            {
                PageIndex = request.PageIndex,
                PageSize = request.PageSize,
            };
            try
            {
                var queryRaw = _dbContext.TblUsers.Where(x => x.IsVerified.HasValue && x.IsVerified.Value && !string.IsNullOrEmpty(x.Username) && !x.Username.Equals(username)).AsQueryable();

                if (!string.IsNullOrEmpty(request.Name))
                    queryRaw = queryRaw.Where(x => !string.IsNullOrEmpty(x.Fullname) && x.Fullname.Contains(request.Name)).AsQueryable();


                response.TotalCount = queryRaw.Count();
                queryRaw = queryRaw.OrderByDescending(x => x.CreatedDate).Skip((request.PageIndex - 1) * request.PageSize).Take(request.PageSize).AsQueryable();

                List<SearchUsersTransactionResponseModel> lstDataResponse =
                    await queryRaw.Select(x => new SearchUsersTransactionResponseModel
                    {
                        UserId = x.Id,
                        Username = x.Username,
                        Fullname = x.Fullname
                    }).ToListAsync();

                response.Data = lstDataResponse;
                response.SetSuccess();
            }
            catch (Exception)
            {
            }
            return response;
        }
        public async Task<PagingResponseModel<TransactionResponseModel>> GetPagingTransaction(TransactionRequestModel request, string username)
        {
            PagingResponseModel<TransactionResponseModel> response = new()
            {
                PageIndex = request.PageIndex,
                PageSize = request.PageSize,
            };

            try
            {
                List<TransactionResponseModel> lstDataResponse = new();
                var transactions = _dbContext.TblTransactions.Where(x =>
                                                           !string.IsNullOrEmpty(x.TransactionTargetUsername) && !string.IsNullOrEmpty(x.TransactionSourceUsername)
                                                           && (request.Status == -1 || x.Status == request.Status))
                                               .AsQueryable();
                if (request.TransactionType == (int)EnumTransactionType.SendToken)
                {
                    transactions = transactions.Where(x => x.TransactionSourceUsername.ToLower().Trim().Equals(username.ToLower().Trim()) && x.TransactionType == request.TransactionType && x.TransactionSourceUsername.ToLower().Trim().Equals(username.ToLower().Trim()));
                
                }
                else if (request.TransactionType == (int)EnumTransactionType.ReceiveFromSystem
                        || request.TransactionType == (int)EnumTransactionType.AttendedDaily
                        || request.TransactionType == (int)EnumTransactionType.ReceiveToken)
                {
                    transactions = transactions.Where(x => x.TransactionTargetUsername.ToLower().Trim().Equals(username.ToLower().Trim())&& x.TransactionType == request.TransactionType && x.TransactionTargetUsername.ToLower().Trim().Equals(username.ToLower().Trim()));
                }
                else
                {
                    transactions = transactions.Where(x => x.TransactionSourceUsername.ToLower().Trim().Equals(username.ToLower().Trim()));
                }


                response.TotalCount = transactions.Count();
                transactions = transactions.OrderByDescending(x => x.CreatedDate).Skip((request.PageIndex - 1) * request.PageSize).Take(request.PageSize).AsQueryable();
                foreach (var item in transactions)
                {
                    TransactionResponseModel itemResponse = new();
                    #region map model
                    itemResponse.Id = item.Id;
                    itemResponse.TransactionSource = item.TransactionSource;
                    itemResponse.TransactionTarget = item.TransactionTarget;
                    itemResponse.TransactionType = item.TransactionType;
                    itemResponse.Amount = item.Amount;
                    itemResponse.TokenId = item.TokenId;
                    itemResponse.TokenCode = item.TokenCode;
                    itemResponse.Descriptions = item.Descriptions;
                    itemResponse.CreatedDate = item.CreatedDate;
                    #endregion
                    lstDataResponse.Add(itemResponse);
                }
                response.Data = lstDataResponse;
                response.SetSuccess();
            }
            catch (Exception)
            {
                throw;
            }
            return response;
        }
        public async Task<ResponseModel<bool>> SendToken(SendTokenTransactionRequestModel request, string username)
        {
            ResponseModel<bool> response = new();
            try
            {
                var wallets = await _dbContext.TblUsers.Where(x => x.Username == username || x.Id == request.ReceiverId)
                                .Select(u => new UserWalletDetailModel
                                {
                                    Username = u.Username,
                                    UserId = u.Id,
                                    WalletDetails = _dbContext.TblWalletDetails.Where(wd => wd.UserId == u.Id).Select(wd => wd).ToList(),
                                }).ToListAsync();

                if (wallets == null || wallets.Count < 2)
                    return response;

                var userCrr = wallets.Where(x => x.Username.Equals(username)).FirstOrDefault();
                var userReceiver = wallets.Where(x => x.UserId == request.ReceiverId).FirstOrDefault();

                if (userCrr == null || userReceiver == null)
                    return response;

                var userWalletCrr = userCrr.WalletDetails.Where(x => x.TokenId == request.TokenId).FirstOrDefault();
                var userWalletReceiver = userReceiver.WalletDetails.Where(x => x.TokenId == request.TokenId).FirstOrDefault();


                var tokenInfo = _dbContext.TblTokens.Where(x => x.Id == request.TokenId).FirstOrDefault();

                if (userWalletCrr == null || (userWalletCrr != null && userWalletCrr.BalanceAvaiable < request.Amount))
                {
                    response.Message = "Số dư của bạn không đủ để chuyển";
                    return response;
                }
                else
                {
                    userWalletCrr.BalanceAvaiable -= request.Amount;
                    userWalletCrr.Balance -= request.Amount;
                    _dbContext.TblWalletDetails.Update(userWalletCrr);
                }
                //nếu ng nhận chưa có ví => tạo ví và chuyển vào 
                if (userWalletReceiver == null)
                {
                    _dbContext.TblWalletDetails.Add(new TblWalletDetails
                    {
                        UserId = userReceiver.UserId,
                        Username = userReceiver.Username,
                        Price = tokenInfo.Price,
                        TokenId = tokenInfo.Id,
                        TokenName = tokenInfo.TokenName,
                        TokenCode = tokenInfo.TokenCode,
                        TokenAddressBep20 = tokenInfo.ContractCodeBep20,
                        TokenAddressERC20 = tokenInfo.ContractCodeERC20,
                        BalanceAvaiable = request.Amount,
                        Balance = request.Amount,
                    });
                }
                else
                {
                    userWalletReceiver.Balance += request.Amount;
                    userWalletReceiver.BalanceAvaiable += request.Amount;
                    _dbContext.TblWalletDetails.Update(userWalletReceiver);
                }

                _dbContext.TblTransactions.Add(new TblTransactions
                {
                    TransactionSource = userCrr.UserId,
                    TransactionSourceUsername = userCrr.Username,
                    TransactionTarget = userReceiver.UserId,
                    TransactionTargetUsername = userReceiver.Username,
                    TransactionType = (int)EnumTransactionType.SendToken,
                    Descriptions = "Chuyển " + request.Amount + " " + tokenInfo.TokenCode + " tới " + userReceiver.Username,
                    Amount = request.Amount,
                    TokenCode = tokenInfo.TokenCode,
                    TokenId = tokenInfo.Id,
                    Status = (int)EnumTransactionStatus.Success
                });

                _dbContext.TblTransactions.Add(new TblTransactions
                {
                    TransactionSource = userReceiver.UserId,
                    TransactionSourceUsername = userReceiver.Username,
                    TransactionTarget = userCrr.UserId,
                    TransactionTargetUsername = userCrr.Username,
                    TransactionType = (int)EnumTransactionType.ReceiveToken,
                    Descriptions = "Nhận " + request.Amount + " " + tokenInfo.TokenCode + " từ " + userCrr.Username,
                    Amount = request.Amount,
                    TokenCode = tokenInfo.TokenCode,
                    TokenId = tokenInfo.Id,
                    Status = (int)EnumTransactionStatus.Success
                });

                await _dbContext.SaveChangesAsync();
                response.Data = true;
                response.SetSuccess();
            }
            catch (Exception ex)
            {
            }
            return response;
        }
        public async Task<ResponseModel<bool>> ClaimEarnToken(decimal amountClaim = 0, string username = "")
        {
            ResponseModel<bool> response = new();
            try
            {
                var userInfor = _dbContext.TblUsers.Where(x => x.Username == username).Select(x => new { UserId = x.Id, Username = x.Username }).FirstOrDefault();
                var tokenInfo = _dbContext.TblTokens.Where(x => x.Id == (int)EnumToken.WREGE).FirstOrDefault();
                var wallets = _dbContext.TblWalletDetails
                    .Where(x => x.Username == username && (x.TokenId == (int)EnumToken.WREG || x.TokenId == (int)EnumToken.WREGE)).ToList();
                var earnWallet = wallets.Where(x => x.TokenId == (int)EnumToken.WREGE).FirstOrDefault();
                var wregWallet = wallets.Where(x => x.TokenId == (int)EnumToken.WREG).FirstOrDefault();


                if (earnWallet == null)
                {
                    response.Message = "Không tồn tại ví WREGE vui lòng kiểm tra lại";
                    return response;
                }
                else if (earnWallet.BalanceAvaiable < amountClaim)
                {
                    response.Message = "Số dư tại ví WREGE không đủ vui lòng kiểm tra lại";
                    return response;
                }

                if (wregWallet == null)
                {
                    _dbContext.TblWalletDetails.Add(new TblWalletDetails
                    {
                        UserId = userInfor.UserId,
                        Username = userInfor.Username,
                        Price = tokenInfo.Price,
                        TokenId = tokenInfo.Id,
                        TokenName = tokenInfo.TokenName,
                        TokenCode = tokenInfo.TokenCode,
                        TokenAddressBep20 = tokenInfo.ContractCodeBep20,
                        TokenAddressERC20 = tokenInfo.ContractCodeERC20,
                        BalanceAvaiable = amountClaim,
                        Balance = amountClaim,
                    });
                }
                else
                {
                    wregWallet.Balance += amountClaim;
                    wregWallet.BalanceAvaiable += amountClaim;
                    _dbContext.TblWalletDetails.Update(wregWallet);
                }

                _dbContext.TblTransactions.Add(new TblTransactions
                {
                    TransactionSource = userInfor.UserId,
                    TransactionSourceUsername = userInfor.Username,
                    TransactionTarget = userInfor.UserId,
                    TransactionTargetUsername = userInfor.Username,
                    TransactionType = (int)EnumTransactionType.SendToken,
                    Descriptions = "Chuyển " + amountClaim + " " + tokenInfo.TokenCode + " tới Ví cá nhân",
                    Amount = amountClaim,
                    TokenCode = tokenInfo.TokenCode,
                    TokenId = tokenInfo.Id,
                    Status = (int)EnumTransactionStatus.Success
                });

                _dbContext.TblTransactions.Add(new TblTransactions
                {
                    TransactionSource = userInfor.UserId,
                    TransactionSourceUsername = userInfor.Username,
                    TransactionTarget = userInfor.UserId,
                    TransactionTargetUsername = userInfor.Username,
                    TransactionType = (int)EnumTransactionType.SendToken,
                    Descriptions = "Nhận " + amountClaim + " " + tokenInfo.TokenCode + " từ ví Earning",
                    Amount = amountClaim,
                    TokenCode = tokenInfo.TokenCode,
                    TokenId = tokenInfo.Id,
                    Status = (int)EnumTransactionStatus.Success
                });
                await _dbContext.SaveChangesAsync();
                response.Data = true;
                response.SetSuccess();
            }
            catch (Exception ex)
            {
            }
            return response;
        }


    }
}
