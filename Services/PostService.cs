﻿using FFE.Common;
using FFE.Entities;
using FFE.Models;
using FFE.Models.Posts;
using FFE.Services.Interfaces;

using Microsoft.EntityFrameworkCore;

using System.Text.Json;

using static FFE.Common.EnumBase;

namespace FFE.Services
{
    public interface IPostService
    {
        Task<PagingResponseModel<PostResponseModel>> GetPagingPost(PostRequestModel request);
    }

    public class PostService : IPostService
    {
        FFEContext _dbContext;
        public PostService(FFEContext dbContext)
        {
            _dbContext = dbContext;
        }


        public async Task<PagingResponseModel<PostResponseModel>> GetPagingPost(PostRequestModel request)
        {
            PagingResponseModel<PostResponseModel> response = new()
            {
                PageIndex = request.PageIndex, 
                PageSize = request.PageSize,    
            };

            try
            {
                List<PostResponseModel> lstDataResponse = new();
                var posts = _dbContext.TblPosts.Where(x => x.CategoriId == (int)EnumCategories.Post 
                                                            && (request.Status == -1 || x.Status == request.Status)
                                                            && (string.IsNullOrEmpty(request.TitleSearch) || (!string.IsNullOrEmpty(x.Title) && x.Title.Contains(request.TitleSearch))))
                                                .AsQueryable();

                response.TotalCount = posts.Count();
                posts = posts.OrderByDescending(x => x.CreatedDate).Skip((request.PageIndex - 1) * request.PageSize).Take(request.PageSize).AsQueryable();

                foreach (var item in posts)
                {
                    PostResponseModel itemResponse = new();
                    #region parse img
                    List<string>? lstStrImg = !string.IsNullOrEmpty(item.Images) ? JsonSerializer.Deserialize<List<string>>(item.Images) : new List<string>();
                    List<string> lstResultImg = new();
                    string thumb = "";
                    if (lstStrImg != null && lstStrImg.Count > 0)
                    {
                        foreach (string str in lstStrImg)
                        {
                            var linkImg = "";
                            if (!str.Contains("http"))
                                linkImg = Constant.URL_IMAGE + str;
                            else linkImg = str;

                            lstResultImg.Add(linkImg);
                        }
                    }

                    if (string.IsNullOrEmpty(item.Thumbnail))
                        thumb = Constant.URL_IMAGE_THUMB_DEFAULT;
                    else if (!item.Thumbnail.Contains("http"))
                        thumb = Constant.URL_IMAGE + item.Thumbnail;
                    else
                        thumb = item.Thumbnail;
                    #endregion
                    #region map model
                    itemResponse.Id = item.Id;
                    itemResponse.Title = item.Title;
                    itemResponse.Summary = item.Summary;
                    itemResponse.Contents = item.Contents;
                    itemResponse.Status = item.Status;
                    itemResponse.LikedCount = item.LikedCount;
                    itemResponse.SharedCount = item.SharedCount;
                    itemResponse.CommentedCount = item.CommentedCount;
                    itemResponse.Thumbnail = thumb;
                    itemResponse.CategoriId = item.CategoriId;
                    itemResponse.Images = lstResultImg;
                    itemResponse.CreatedDate = item.CreatedDate;
                    #endregion 
                    lstDataResponse.Add(itemResponse);
                }
                response.Data = lstDataResponse;
                response.SetSuccess();
            }
            catch (Exception)
            {
                throw;
            }
            return response;
        }
    }
}
