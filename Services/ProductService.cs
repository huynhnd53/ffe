﻿using FFE.Entities;
using FFE.Models.Posts;
using FFE.Models;
using static FFE.Common.EnumBase;
using Microsoft.Extensions.Hosting;
using System.Text.Json;
using FFE.Common;

namespace FFE.Services
{
    public interface IProductService
    {
        Task<PagingResponseModel<ProductResponseModel>> GetPagingProduct(ProductRequestModel request);
    }
    public class ProductService : IProductService
    {
        FFEContext _dbContext;
        public ProductService(FFEContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<PagingResponseModel<ProductResponseModel>> GetPagingProduct(ProductRequestModel request)
        {
            PagingResponseModel<ProductResponseModel> response = new()
            {
                PageIndex = request.PageIndex,
                PageSize = request.PageSize,
            };
            try
            {
                List<ProductResponseModel> lstDataResponse = new();
                var products = _dbContext.TblProducts.Where(x => (request.Status == -1 || x.Status == request.Status)
                                                           && (string.IsNullOrEmpty(request.ProductNameSearch) || (!string.IsNullOrEmpty(x.ProductName) && x.ProductName.Contains(request.ProductNameSearch))))
                                               .AsQueryable();
                response.TotalCount = products.Count();
                products = products.OrderByDescending(x => x.CreatedDate).Skip((request.PageIndex - 1) * request.PageSize).Take(request.PageSize).AsQueryable();

                foreach (var item in products)
                {
                    ProductResponseModel itemResponse = new();
                    #region parse img
                    List<string>? lstStrImg = !string.IsNullOrEmpty(item.Images) ? JsonSerializer.Deserialize<List<string>>(item.Images) : new List<string>();
                    List<string> lstResultImg = new();
                    string thumb = "";
                    if (lstStrImg != null && lstStrImg.Count > 0)
                    {
                        foreach (string str in lstStrImg)
                        {
                            var linkImg = "";
                            if (!str.Contains("http"))
                                linkImg = Constant.URL_IMAGE + str;
                            else linkImg = str;

                            lstResultImg.Add(linkImg);
                        }
                    }

                    if (string.IsNullOrEmpty(item.Thumbnail))
                        thumb = Constant.URL_IMAGE_THUMB_DEFAULT;
                    else if (!item.Thumbnail.Contains("http"))
                        thumb = Constant.URL_IMAGE + item.Thumbnail;
                    else
                        thumb = item.Thumbnail;
                    #endregion
                    #region map model
                    itemResponse.Id = item.Id;
                    itemResponse.ProductName = item.ProductName;
                    itemResponse.Descriptions = item.Descriptions;
                    itemResponse.Contents = item.Contents;
                    itemResponse.ProductPrice = item.ProductPrice;
                    itemResponse.Discount = item.Discount;
                    itemResponse.CategoryId = item.CategoryId;
                    itemResponse.Status = item.Status;
                    itemResponse.Unit = item.Unit;
                    itemResponse.Thumbnail = thumb;
                    itemResponse.Images = lstResultImg;
                    #endregion 
                    lstDataResponse.Add(itemResponse);
                }
                response.Data = lstDataResponse;
                response.SetSuccess();
            }
            catch (Exception)
            {
                throw;
            }
            return response;
        }
    }
}
