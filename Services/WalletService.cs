﻿using Azure;

using FFE.Entities;
using FFE.Models;
using FFE.Models.Wallet;

using Microsoft.EntityFrameworkCore;

namespace FFE.Services
{
    public interface IWalletService
    {
        Task<ResponseModel<WalletResponseModel>> MyWalletDetail(string username);
        Task<ResponseModel<List<WalletDetailResponseModel>>> MyListWalletDetail(string username);
    }

    public class WalletService : IWalletService
    {
        FFEContext _dbContext;
        public WalletService(FFEContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<ResponseModel<WalletResponseModel>> MyWalletDetail(string username)
        {
            ResponseModel<WalletResponseModel> response = new();
            try
            {
                var walletDetail = await (from u in _dbContext.TblUsers
                                          join wd in _dbContext.TblWalletDetails on u.Id equals wd.UserId into wds
                                          from wd in wds.DefaultIfEmpty()
                                          where
                                             u.Username == username
                                          select new WalletDetailResponseModel
                                          {
                                              Balance = wd.Balance ?? 0,
                                              BalanceAvaiable = wd.BalanceAvaiable ?? 0,
                                              TokenCode = wd.TokenCode ?? "",
                                              TokenId = wd.TokenId ?? 0,
                                              TokenName = wd.TokenName ?? "",
                                              Price = wd.Price ?? 0,
                                          }).ToListAsync();

                response.Data = new WalletResponseModel
                {
                    Balance = walletDetail.Sum(x => x.Balance * x.Price),
                    BalanceAvaiable = walletDetail.Sum(x => x.BalanceAvaiable * x.Price),
                    WalletDetails = walletDetail.Any(x => x.TokenId > 0) ? walletDetail : null
                };
                response.SetSuccess();
            }
            catch (Exception ex)
            {
            }
            return response;
        }

        public async Task<ResponseModel<List<WalletDetailResponseModel>>> MyListWalletDetail(string username)
        {
            ResponseModel<List<WalletDetailResponseModel>> response = new();
            try
            {
                var walletDetail = await (from u in _dbContext.TblUsers
                                          join wd in _dbContext.TblWalletDetails on u.Id equals wd.UserId into wds
                                          from wd in wds.DefaultIfEmpty()
                                          where
                                             u.Username == username
                                          select new WalletDetailResponseModel
                                          {
                                              Balance = wd.Balance ?? 0,
                                              BalanceAvaiable = wd.BalanceAvaiable ?? 0,
                                              TokenCode = wd.TokenCode ?? "",
                                              TokenId = wd.TokenId ?? 0,
                                              TokenName = wd.TokenName ?? "",
                                              Price = wd.Price ?? 0,
                                          }).ToListAsync();
                if(walletDetail != null && walletDetail.Any(x => x.TokenId > 0)) 
                    response.Data = walletDetail;

                response.SetSuccess();
            }
            catch (Exception ex)
            {
            }
            return response;
        }
    }
}
